/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_SESSION_USER_HH_2D88FC03966F49C4A9C79E92F88896FF
#define GET_SESSION_USER_HH_2D88FC03966F49C4A9C79E92F88896FF

#include "include/liblogger/log_entry.hh"
#include "include/liblogger/session_ident.hh"
#include "src/session_user.hh"

#include "libpg/pg_transaction.hh"

#include <exception>

namespace Fred {
namespace LibLogger {

SessionUser get_session_user(const LibPg::PgTransaction& _transaction, const SessionIdent& _session_ident);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

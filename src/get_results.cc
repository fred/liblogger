/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/get_results.hh"

#include "include/liblogger/exceptions.hh"

#include "src/util.hh"

#include "libpg/query.hh"

namespace Fred {
namespace LibLogger {

GetResultsReply get_results(
        const LibPg::PgTransaction& _transaction,
        const LogEntry::ServiceName& _log_entry_service)
{
    GetResultsReply reply;

    const auto service_id = get_service_id_by_name(_transaction, _log_entry_service);

    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::ResultName>() << "name" <<
                        LibPg::item<LogEntry::ResultCode>() << "result_code "
              "FROM result_code "
             "WHERE service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer();
            // clang-format on

    const auto db_result = exec(_transaction, query, service_id);

    for (const auto& row : db_result)
    {
        const auto result_name = row.get<LogEntry::ResultName>();
        const auto result_code = row.get<LogEntry::ResultCode>();
        reply.result_code_map[result_name] = result_code;
    }

    return reply;
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/create_log_entry.hh"

#include "src/get_optional.hh"
#include "src/get_session_user.hh"
#include "src/log_entry.hh"
#include "src/util.hh"

#include "libpg/query.hh"

#include "liblog/liblog.hh"

#include <boost/format.hpp>

#include <tuple>

namespace Fred {
namespace LibLogger {

namespace {

decltype(auto) insert_log_entry(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LibStrong::Optional<LogEntry::SourceIp>& _source_ip,
        const LogEntry::IsMonitoring& _is_monitoring,
        const LogEntry::ServiceId& _service_id,
        const LibStrong::Optional<LogEntry::LogEntryTypeId>& _log_entry_type_id,
        const LibStrong::Optional<LogEntry::SessionId>& _session_id,
        const LibStrong::Optional<LogEntry::UserName>& _session_user_name,
        const LibStrong::Optional<LogEntry::UserId>& _session_user_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "INSERT INTO request ("
               "time_begin, "
               "source_ip, "
               "service_id, "
               "request_type_id, "
               "session_id, "
               "user_name, "
               "is_monitoring, "
               "user_id) "
            "VALUES ("
               "CURRENT_TIMESTAMP, " <<
                LibPg::parameter<LibStrong::Optional<LogEntry::SourceIp>>().as_type("INET") << ", " <<
                LibPg::parameter<LogEntry::ServiceId>().as_integer() << ", " <<
                LibPg::parameter<LibStrong::Optional<LogEntry::LogEntryTypeId>>().as_integer() << ", " <<
                LibPg::parameter<LibStrong::Optional<LogEntry::SessionId>>().as_big_int() << ", " <<
                // user_name.as_text() should fit into VARCHAR(255), because it was taken from session table as VARCHAR(255)
                LibPg::parameter<LogEntry::UserName>().as_text() << ", " <<
                LibPg::parameter<LogEntry::IsMonitoring>().as_boolean() << ", " <<
                LibPg::parameter<LibStrong::Optional<LogEntry::UserId>>().as_integer() << ")";
            // clang-format on

    LibPg::PgResultNoData{
            exec(_rw_transaction,
                    query,
                    {_source_ip,
                            _service_id,
                            _log_entry_type_id,
                            _session_id,
                            [&]() { return _session_user_name != LogEntry::UserName::nullopt ? *_session_user_name
                                                                                             : LogEntry::UserName{""};}(), // optional user_name represented by empty string in db
                            _is_monitoring,
                            _session_user_id})};

    // get the id of the above INSERT command using SELECT CURRVAL, because there is a trigger on insert to request which does not support RETURNING clause
    const auto db_result =
            exec(_rw_transaction,
                    // clang-format off
                    LibPg::make_query() <<
                    "SELECT" << LibPg::item<LogEntry::LogEntryId>() << "CURRVAL('request_id_seq'::REGCLASS)" <<
                                LibPg::item<LogEntry::TimeBegin>() << "CURRENT_TIMESTAMP::TIMESTAMP");
                    // clang-format on

    return std::make_tuple(
            db_result.front().get<LogEntry::LogEntryId>(),
            db_result.front().get<LogEntry::TimeBegin>());
}

} // namespace Fred::LibLogger::{anonymous}

CreateLogEntryReply create_log_entry(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LibStrong::Optional<LogEntry::SourceIp>& _ip,
        const LogEntry::IsMonitoring& _is_monitoring,
        const LogEntry::ServiceName& _log_entry_service,
        const LibStrong::Optional<LogEntry::Content>& _log_entry_content,
        const std::vector<LogEntryProperty>& _log_entry_properties,
        const std::vector<ObjectReferences>& _object_references,
        const LogEntry::LogEntryTypeName& _log_entry_type,
        const boost::optional<SessionIdent>& _session_ident)
{
    LibLog::SetContext session_ctx{
            boost::str(boost::format("session-%1%") % (_session_ident != boost::none ? std::to_string(*(_session_ident->id)) : "N/A"))};

    const auto log_entry_service_id = get_service_id_by_name(_rw_transaction, _log_entry_service);

    const auto log_entry_type_id = get_log_entry_type_id_by_name(_rw_transaction, _log_entry_type, log_entry_service_id);

    const auto session_user =
            _session_ident != boost::none
                    ? boost::optional<SessionUser>{get_session_user(_rw_transaction, *_session_ident)}
                    : boost::none;

    const auto new_log_entry_details =
            insert_log_entry(
                    _rw_transaction,
                    _ip,
                    _is_monitoring,
                    log_entry_service_id,
                    log_entry_type_id,
                    {_session_ident != boost::none, [&]() { return _session_ident->id; }},
                    {session_user != boost::none, [&]() { return session_user->name; }},
                    session_user != boost::none ? session_user->id
                                                : LogEntry::UserId::nullopt);

    const auto log_entry_id = std::get<LogEntry::LogEntryId>(new_log_entry_details);
    const auto time_begin = std::get<LogEntry::TimeBegin>(new_log_entry_details);

    LibLog::SetContext log_entry_ctx{boost::str(boost::format("request-%1%") % log_entry_id)};

    if (_log_entry_content != LogEntry::Content::nullopt && !(*_log_entry_content)->empty())
    {
        static constexpr auto is_not_response = LogEntry::IsResponse{false};
        LogEntry::insert_log_entry_data(
                _rw_transaction,
                time_begin,
                log_entry_service_id,
                _is_monitoring,
                log_entry_id,
                *_log_entry_content,
                is_not_response);
    }

    for (const auto& log_entry_property : _log_entry_properties)
    {
        if (log_entry_property.property_values.empty())
        {
            LIBLOG_WARNING("unexpected number of property values: input contains 0 property values of type \"{}\"", *log_entry_property.property_type);
        }
        const auto property_name_id = get_log_entry_property_id_by_name(_rw_transaction, log_entry_property.property_type);
        for (const auto& property : log_entry_property.property_values)
        {
            static constexpr auto is_not_output = LogEntry::IsOutput{false};
            const auto inserted_property_id =
                    LogEntry::insert_log_entry_property_as_parent(
                            _rw_transaction,
                            time_begin,
                            log_entry_service_id,
                            _is_monitoring,
                            log_entry_id,
                            property_name_id,
                            property.property_value,
                            is_not_output);

            for (const auto& child : property.children)
            {
                const auto child_property_type = child.first;
                for (const auto& child_property_value : child.second)
                {
                    const auto child_property_name_id = get_log_entry_property_id_by_name(_rw_transaction, child_property_type);
                    LogEntry::insert_log_entry_property_as_child(
                            _rw_transaction,
                            time_begin,
                            log_entry_service_id,
                            _is_monitoring,
                            log_entry_id,
                            child_property_name_id,
                            child_property_value,
                            is_not_output,
                            inserted_property_id);
                }
            }
        }
    }

    for (const auto& object_reference : _object_references)
    {
        if (object_reference.object_reference_ids.empty())
        {
            LIBLOG_WARNING("unexpected number of object references: input contains 0 references of type \"{}\"", *object_reference.object_type);
        }
        for (const auto& object_reference_id : object_reference.object_reference_ids)
        {
            const auto log_entry_object_type_id = get_log_entry_object_type_id_by_name(_rw_transaction, LogEntry::LogEntryObjectTypeName{object_reference.object_type});
            LogEntry::insert_object_reference(
                    _rw_transaction,
                    time_begin,
                    log_entry_service_id,
                    _is_monitoring,
                    log_entry_id,
                    log_entry_object_type_id,
                    object_reference_id);
        }
    }

    CreateLogEntryReply reply{
            LogEntryIdent{
                    log_entry_id,
                    time_begin,
                    _log_entry_service,
                    _is_monitoring}};
    return reply;
}

} // namespace Fred::LibLogger
} // namespace Fred

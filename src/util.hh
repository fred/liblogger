/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef UTIL_HH_E87E01ADA76E42238F195AE4523877AB
#define UTIL_HH_E87E01ADA76E42238F195AE4523877AB

#include "include/liblogger/log_entry.hh"

#include "libpg/pg_transaction.hh"

#include <boost/asio/ip/address.hpp>

namespace Fred {
namespace LibLogger {

bool is_valid(const LogEntry::ServiceName& _service_name);

LogEntry::ServiceId get_service_id_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::ServiceName& _service_name);

LogEntry::LogEntryObjectTypeId get_log_entry_object_type_id_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::LogEntryObjectTypeName& _log_entry_object_type_name);

LogEntry::LogEntryTypeId get_log_entry_type_id_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::LogEntryTypeName& _log_entry_type_name, const LogEntry::ServiceId& _service_id);

/**
 * If the property is not found, a new property of the given type is created and its id returned.
 */
LogEntry::PropertyNameId get_log_entry_property_id_by_name(const LibPg::PgRwTransaction& _rw_transaction, const LogEntry::PropertyType& _property_type);

LogEntry::ResultCode get_result_code_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::ResultName& _result_name, const LogEntry::ServiceId& _service_id);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

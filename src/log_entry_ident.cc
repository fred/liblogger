/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/log_entry_ident.hh"
#include "include/liblogger/exceptions.hh"

#include <cctype>
#include <utility>


namespace Fred {
namespace LibLogger {

namespace {

LogEntry::LogEntryId& validate(LogEntry::LogEntryId& _id)
{
    if (_id < LogEntry::LogEntryId{1ull})
    {
        throw InvalidLogEntryIdent{};
    }
    return _id;
}

LogEntry::TimeBeginDate& validate(LogEntry::TimeBeginDate& _time_begin)
{
    if (_time_begin->is_special())
    {
        throw InvalidLogEntryIdent{};
    }
    return _time_begin;
}

LogEntry::ServiceName& validate(LogEntry::ServiceName& _service_name)
{
    if (_service_name->empty())
    {
        throw InvalidLogEntryIdent{};
    }
    return _service_name;
}

LogEntry::TimeBeginDate extract_date(const LogEntry::TimeBegin& time)
{
    return LogEntry::TimeBeginDate{time->date()};
}

} // namespace Fred::LibLogger::{anonymous}

LogEntryIdent::LogEntryIdent(
        LogEntry::LogEntryId _id,
        LogEntry::TimeBegin _time_begin,
        LogEntry::ServiceName _service_name,
        LogEntry::IsMonitoring _monitoring_status)
    : LogEntryIdent{std::move(_id),
                    extract_date(_time_begin),
                    std::move(_service_name),
                    std::move(_monitoring_status)}
{
}

LogEntryIdent::LogEntryIdent(
        LogEntry::LogEntryId _id,
        LogEntry::TimeBeginDate _time_begin,
        LogEntry::ServiceName _service_name,
        LogEntry::IsMonitoring _monitoring_status)
    : id{std::move(validate(_id))},
      time_begin{std::move(validate(_time_begin))},
      service_name{std::move(validate(_service_name))},
      monitoring_status{std::move(_monitoring_status)}
{
}

} // namespace Fred::LibLogger
} // namespace Fred

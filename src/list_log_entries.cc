/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/list_log_entries.hh"

#include "include/liblogger/exceptions.hh"

#include "src/util.hh"
#include "src/log_entry.hh"

#include "libpg/extendable_query_arguments.hh"
#include "libpg/pg_rw_transaction.hh"
#include "libpg/pg_result.hh"

#include "liblog/liblog.hh"

#include <string>

namespace Fred {
namespace LibLogger {

namespace {

bool operator<=(const LogEntry::TimeEnd& _lhs, const LogEntry::TimeBegin& _rhs)
{
    return *_lhs <= *_rhs;
}

} // namespace Fred::LibLogger::{anonymous}

ListLogEntriesReply list_log_entries(
        const LibPg::PgTransaction& _transaction,
        const LogEntry::ServiceName& _service,
        const LogEntry::TimeBegin& _from,
        const LogEntry::TimeEnd& _to,
        unsigned int _count_limit,
        const LibStrong::Optional<LogEntry::UserName>& _username,
        const LibStrong::Optional<LogEntry::UserId>& _user_id,
        const boost::optional<ObjectReference>& _object_reference,
        const std::vector<LogEntry::LogEntryTypeName>& _log_entry_types,
        const std::map<LogEntry::PropertyType, std::vector<LogEntry::PropertyValue>>& _properties)
{
    if (_from->is_special())
    {
        throw InvalidTimestampFrom{};
    }
    if (_to->is_special())
    {
        throw InvalidTimestampTo{};
    }
    else if (_to <= _from)
    {
        throw InvalidTimestampRange{};
    }
    if (_count_limit < 1)
    {
        throw InvalidCountLimit{};
    }

    const auto service_id = get_service_id_by_name(_transaction, _service);

    LibPg::ExtendableQueryArguments query_arguments(*service_id, *_from, *_to);

    std::string query =
            // clang-format off
            "SELECT r.id AS request_id, r.time_begin::DATE "
              "FROM request r "
              "JOIN request_type rt ON rt.id = r.request_type_id ";
            // clang-format on

    const auto has_object_reference =
            (_object_reference != boost::none &&
             !_object_reference->object_type->empty() &&
             !_object_reference->object_ident->empty());

    if (has_object_reference)
    {
        // clang-format off
        query += "LEFT JOIN request_object_ref ror ON ror.request_id = r.id "
                  "AND NOT ror.request_monitoring "
                  "AND ror.request_service_id = " + query_arguments.parameter(0).as_big_int() + " "
                  "AND ror.request_time_begin >= " + query_arguments.parameter(1).as_timestamp() + " "
                  "AND ror.request_time_begin < " + query_arguments.parameter(2).as_timestamp() + " "
                 "LEFT JOIN request_object_type rot ON rot.id = ror.object_type_id ";
        // clang-format on
    }
    if (!_properties.empty())
    {
        // clang-format off
        query += "JOIN request_property_value rpv ON rpv.request_id = r.id "
                  "AND NOT rpv.request_monitoring "
                  "AND rpv.request_service_id = " + query_arguments.parameter(0).as_big_int() + " "
                  "AND rpv.request_time_begin >= " + query_arguments.parameter(1).as_timestamp() + " "
                  "AND rpv.request_time_begin < " + query_arguments.parameter(2).as_timestamp() + " "
                 "JOIN request_property_name rpn ON rpn.id = rpv.property_name_id ";
        // clang-format on
    }
    // clang-format off
    query += "WHERE NOT r.is_monitoring "
               "AND r.time_begin >= " + query_arguments.parameter(1).as_timestamp() + " "
               "AND r.time_begin < " + query_arguments.parameter(2).as_timestamp() + " "
               "AND r.service_id = " + query_arguments.parameter(0).as_integer() + " ";
    // clang-format on

    if (_username != LogEntry::UserName::nullopt && !(*_username)->empty())
    {
        query += "AND r.user_name = " + query_arguments.add(**_username).as_text() + " ";
    }

    static constexpr auto empty_user_id = LogEntry::UserId{0ull};
    if (_user_id != LogEntry::UserId::nullopt && *_user_id != empty_user_id)
    {
        query += "AND r.user_id = " + query_arguments.add(**_user_id).as_integer() + " ";
    }

    std::string sql_log_entry_types =
            "AND LOWER(rt.name) IN (";
    std::string delimiter;
    bool has_one_type_at_least = false;
    for (const auto& type : _log_entry_types)
    {
        if (!type->empty())
        {
            sql_log_entry_types += delimiter + "LOWER(" + query_arguments.add(type).as_text() + ")";
            if (delimiter.empty())
            {
                delimiter = ", ";
            }
            has_one_type_at_least = true;
        }
    }
    sql_log_entry_types += ") ";
    if (has_one_type_at_least)
    {
        query += sql_log_entry_types;
    }

    if (has_object_reference)
    {
        const auto object_ident_variant = LogEntry::get_ident_variants_from(_object_reference->object_ident);
        if (*std::get<LogEntry::CorbaImplCompatibility>(object_ident_variant))
        {
            // for old corba implementation, if object_ident is convertible to int, use it as object_id, not object_ident
            query += "AND ror.object_id = " + query_arguments.add(std::get<LogEntry::ObjectId>(object_ident_variant)).as_integer() + " ";
        }
        else
        {
            // for new grpc implementation, object_ident is NOT convertible to int, use it as is
            query += "AND ror.object_ident = " + query_arguments.add(_object_reference->object_ident).as_text() + " ";
        }
        query += "AND LOWER(rot.name) = LOWER(" + query_arguments.add(_object_reference->object_type).as_text() + ") ";
    }

    const auto joined_property_values =
            [&](const auto& property_values)
            {
                std::string sql;
                std::for_each(begin(property_values), end(property_values), [&](auto&& property_value)
                {
                    if (!sql.empty())
                    {
                        sql += ", ";
                    }
                    sql = sql + query_arguments.add(property_value).as_text();
                });
                return sql;
            };

    const auto joined_properties_sql =
            [&](const auto& properties)
            {
                std::string sql;
                std::for_each(begin(properties), end(properties), [&](auto&& property)
                {
                    if (!sql.empty())
                    {
                        sql += " OR ";
                    }
                    sql += "(rpn.name = " + query_arguments.add(*property.first).as_text() + " "
                           "AND rpv.value IN (" + joined_property_values(property.second) + "))";
                });
                return sql;
            };

    if (!_properties.empty())
    {
        query += "AND (" + joined_properties_sql(_properties) + ") ";
    }

    query += "GROUP BY r.id "
             "ORDER BY r.time_begin DESC "
             "LIMIT " + std::to_string(_count_limit);

    ListLogEntriesReply reply;
    try
    {
        const LibPg::PgResultTuples db_result = exec(_transaction, query, query_arguments);

        for (const auto row : db_result)
        {
            const auto row_values = row.as<
                    std::tuple<
                            LogEntry::LogEntryId::ValueType,
                            LogEntry::TimeBeginDate::ValueType
                    >>();

            reply.log_entry_ids.emplace_back(
                    LogEntry::LogEntryId{std::get<0>(row_values)},
                    LogEntry::TimeBeginDate{std::get<1>(row_values)},
                    _service,
                    LogEntry::IsMonitoring{false});
        }
    }
    catch (const std::exception& e)
    {
        LIBLOG_WARNING(e.what());
        throw ListLogEntriesException{};
    }
    return reply;
}

} // namespace Fred::LibLogger
} // namespace Fred

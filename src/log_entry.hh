/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOG_ENTRY_HH_F267D5601CC64B92BCC5F72CCA0BF614
#define LOG_ENTRY_HH_F267D5601CC64B92BCC5F72CCA0BF614

#include "liblogger/log_entry.hh"

namespace Fred {
namespace LibLogger {
namespace LogEntry {

bool is_valid(const ServiceName& _service_name);

void insert_log_entry_data(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const Content& _content,
        const IsResponse& _is_response);

PropertyId insert_log_entry_property_as_parent(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const PropertyNameId& _property_name_id,
        const PropertyValue& _property_value,
        const IsOutput& _is_output);

void insert_log_entry_property_as_child(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const PropertyNameId& _property_name_id,
        const PropertyValue& _property_value,
        const IsOutput& _is_output,
        const PropertyId& _parent_id);

void insert_object_reference(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const LogEntryObjectTypeId& _log_entry_object_type_id,
        const ObjectIdent& _object_id);

std::tuple<ObjectId, boost::optional<ObjectIdent>, CorbaImplCompatibility> get_ident_variants_from(const ObjectIdent& _object_ident);

} // namespace Fred::LibLogger::LogEntry
} // namespace Fred::LibLogger
} // namespace Fred

namespace boost {
namespace asio {
namespace ip {

std::string wrap_into_psql_representation(const boost::asio::ip::address& value);

boost::asio::ip::address unwrap_from_psql_representation(const boost::asio::ip::address&, const char* src);

} // namespace boost::asio::ip
} // namespace boost::asio
} // namespace boost

#endif

/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/register_object_reference_types.hh"

#include "include/liblogger/exceptions.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

namespace Fred {
namespace LibLogger {

namespace {

void insert_object_reference_type(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::LogEntryObjectTypeName& _object_type)
{
    if (!is_valid(_object_type))
    {
        throw InvalidObjectType{};
    }

    const auto query = LibPg::make_query() <<
            // clang-format off
             "INSERT "
               "INTO request_object_type (name) "
             "SELECT " << LibPg::parameter<LogEntry::LogEntryObjectTypeName>().as_text() << " "
              "WHERE NOT EXISTS (SELECT "
                                  "FROM request_object_type "
                                 "WHERE LOWER(name) = LOWER(" << LibPg::parameter<LogEntry::LogEntryObjectTypeName>().as_text() << ")) "
                 "ON CONFLICT DO NOTHING";
            // clang-format on
    try
    {
        LibPg::PgResultNoData{
            exec(_rw_transaction, query, _object_type)};
    }
    catch (const LibPg::ExecFailure& e)
    {
        if (e.error_code() == LibPg::SqlState::string_data_right_truncation)
        {
            // the only string that can be truncated from text to varchar(256) is _object_type
            throw InvalidObjectType{};
        }
        throw;
    }
}

} // namespace Fred::LibLogger::{anonymous}

bool is_valid(const LogEntry::LogEntryObjectTypeName& _object_reference_type)
{
    return !_object_reference_type->empty();
}

void register_object_reference_types(
        const LibPg::PgRwTransaction& _rw_transaction,
        const std::vector<LogEntry::LogEntryObjectTypeName>& _object_reference_types)
{
    for (const auto& object_reference_type : _object_reference_types)
    {
        insert_object_reference_type(_rw_transaction, object_reference_type);
    }
}

} // namespace Fred::LibLogger
} // namespace Fred

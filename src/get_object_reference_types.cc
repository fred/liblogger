/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/get_object_reference_types.hh"

#include "libpg/query.hh"

namespace Fred {
namespace LibLogger {

GetObjectReferenceTypesReply get_object_reference_types(
        const LibPg::PgTransaction& _transaction)
{
    GetObjectReferenceTypesReply reply;

    const auto db_result =
            exec(_transaction,
                 LibPg::make_query() <<
                 // clang-format off
                 "SELECT" << LibPg::item<LogEntry::LogEntryObjectTypeName>() << "name "
                   "FROM request_object_type "
                  "ORDER BY name");
                 // clang-format on

    reply.object_reference_types.reserve(*db_result.size());
    for (const auto& row : db_result)
    {
        reply.object_reference_types.emplace_back(*row.get<LogEntry::LogEntryObjectTypeName>());
    }

    return reply;
}

} // namespace Fred::LibLogger
} // namespace Fred

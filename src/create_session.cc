/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/create_session.hh"

#include "include/liblogger/exceptions.hh"

#include "liblog/liblog.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

namespace Fred {
namespace LibLogger {

CreateSessionReply create_session(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::UserName& _user_name,
        const LibStrong::Optional<LogEntry::UserId>& _user_id)
{
    static const auto empty_user_name = LogEntry::UserName{""};
    if (_user_name == empty_user_name)
    {
        throw InvalidUserName{};
    }

    const auto query = LibPg::make_query() <<
                 // clang-format off
                 "INSERT INTO session (user_name, login_date, user_id) "
                 "VALUES (" << LibPg::parameter<LogEntry::UserName>().as_text() << ", "
                              "CURRENT_TIMESTAMP, " <<
                               LibPg::parameter<LibStrong::Optional<LogEntry::UserId>>().as_integer() << ")";
                 // clang-format on

    try
    {
        LibPg::PgResultNoData{exec(_rw_transaction, query, {_user_name, _user_id})};
    }
    catch (const LibPg::ExecFailure& e)
    {
        if (e.error_code() == LibPg::SqlState::string_data_right_truncation)
        {
            // the only string that can be truncated from text to varchar(255) is user_name
            throw InvalidUserName{};
        }
        throw;
    }

    // get the id of the above INSERT command using SELECT CURRVAL, because there is a trigger on insert to session which does not support RETURNING clause
    const auto db_result =
            exec(_rw_transaction,
                    LibPg::make_query() << "SELECT" <<
                    LibPg::item<LogEntry::SessionId>() << "CURRVAL('session_id_seq'::REGCLASS)" <<
                    LibPg::item<LogEntry::SessionLoginTime>() << "CURRENT_TIMESTAMP::TIMESTAMP");

    auto reply =
            CreateSessionReply{
                    SessionIdent{
                            db_result.front().get<LogEntry::SessionId>(),
                            db_result.front().get<LogEntry::SessionLoginTime>()}};

    LIBLOG_INFO("created session with session_id: \"{}\"", *reply.session_ident.id);

    return reply;
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/get_log_entry_types.hh"

#include "src/util.hh"

#include "liblog/context.hh"
#include "liblog/liblog.hh"

#include "libpg/query.hh"

namespace Fred {
namespace LibLogger {

GetLogEntryTypesReply get_log_entry_types(
        const LibPg::PgTransaction& _transaction,
        const LogEntry::ServiceName& _log_entry_service)
{
    GetLogEntryTypesReply reply;

    const auto service_id = get_service_id_by_name(_transaction, _log_entry_service);

    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::LogEntryTypeName>() << "name "
              "FROM request_type "
             "WHERE service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer();
            // clang-format on

    const auto db_result = exec(_transaction, query, service_id);

    reply.log_entry_types.reserve(*db_result.size());
    for (const auto& row : db_result)
    {
        reply.log_entry_types.emplace_back(row.get<LogEntry::LogEntryTypeName>());
    }

    return reply;
}

} // namespace Fred::LibLogger
} // namespace Fred

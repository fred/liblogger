/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/register_results.hh"

#include "include/liblogger/exceptions.hh"

#include "src/util.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

namespace Fred {
namespace LibLogger {

namespace {

struct ResultExists : std::exception
{
    const char* what() const noexcept override
    {
        return "result exists";
    }
};

void check_is_available(
        const LibPg::PgTransaction& _transaction,
        const Result& _result,
        const LogEntry::ServiceId& _service_id)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::ResultCode>() << "result_code" <<
                        LibPg::item<LogEntry::ResultName>() << "name "
              "FROM result_code "
             "WHERE service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer() << " " <<
               "AND (result_code = " << LibPg::parameter<LogEntry::ResultCode>().as_integer() << " " <<
                    "OR LOWER(name) = LOWER(" << LibPg::parameter<LogEntry::ResultName>().as_text() << "))";
            // clang-format on

    const auto db_result = exec(_transaction, query, {_service_id, _result.code, _result.name});

    if (db_result.size() == LibPg::RowIndex{1})
    {
        const auto result_code = db_result.front().get<LogEntry::ResultCode>();
        const auto result_name = db_result.front().get<LogEntry::ResultName>();
        if (result_code == _result.code &&
            result_name == _result.name)
        {
            throw ResultExists{};
        }
    };

    if (!db_result.empty())
    {
        throw InvalidResult{};
    };
}

void insert_result(
        const LibPg::PgRwTransaction& _rw_transaction,
        const Result& _result,
        const LogEntry::ServiceId& _service_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
             "INSERT "
               "INTO result_code (result_code, "
                                 "name, "
                                 "service_id) "
             "SELECT " << LibPg::parameter<LogEntry::ResultCode>().as_integer() << ", " <<
                          LibPg::parameter<LogEntry::ResultName>().as_text() << ", " <<
                          LibPg::parameter<LogEntry::ServiceId>().as_integer() << " "
              "WHERE NOT EXISTS (SELECT "
                                  "FROM result_code "
                                 "WHERE service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer() << " "
                                   "AND (result_code = " << LibPg::parameter<LogEntry::ResultCode>().as_integer() << " "
                                        "OR LOWER(name) = LOWER(" << LibPg::parameter<LogEntry::ResultName>().as_text() << "))) "
                 "ON CONFLICT DO NOTHING";
            // clang-format on

    LibPg::PgResultNoData db_result;
    try
    {
        db_result = exec(_rw_transaction, query, {_result.code, _result.name, _service_id});
    }
    catch (const LibPg::ExecFailure& e)
    {
        if (e.error_code() == LibPg::SqlState::string_data_right_truncation)
        {
            // the only string that can be truncated from text to varchar(64) is _result.name
            throw InvalidResultName{};
        }
        throw;
    }

    if (db_result.get_number_of_rows_affected() == LibPg::RowIndex{0})
    {
        check_is_available(_rw_transaction, _result, _service_id);
    }
}

} // namespace Fred::LibLogger::{anonymous}

void register_results(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::ServiceName& _service,
        const std::vector<Result>& _results)
{
    const auto service_id = get_service_id_by_name(_rw_transaction, _service);

    for (const auto& result : _results)
    {
        try
        {
            insert_result(_rw_transaction, result, service_id);
        }
        catch (const ResultExists&)
        {
        }
    }
}

} // namespace Fred::LibLogger
} // namespace Fred

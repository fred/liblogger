/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/get_log_entry_info.hh"
#include "include/liblogger/exceptions.hh"

#include "src/get_optional.hh"
#include "src/log_entry.hh"
#include "src/util.hh"

#include "liblog/liblog.hh"

#include "libpg/query.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional.hpp>

#include <map>
#include <stdexcept>

namespace Fred {
namespace LibLogger {

namespace {

LogEntryInfo get_basic_info(
        const LibPg::PgTransaction& _transaction,
        const LogEntryIdent& _log_entry_ident,
        const LogEntry::ServiceId& _service_id)
{
    LIBLOG_DEBUG(__func__);

    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::TimeBegin>() << "r.time_begin" <<
                        LibPg::nullable_item<LogEntry::TimeEnd>() << "r.time_end" <<
                        LibPg::nullable_item<LogEntry::SourceIp>() << "r.source_ip" <<
                        LibPg::item<LogEntry::ServiceName>() << "s.name" <<
                        LibPg::nullable_item<LogEntry::ResultCode>() << "rc.result_code" <<
                        LibPg::nullable_item<LogEntry::ResultName>() << "rc.name" <<
                        LibPg::nullable_item<LogEntry::LogEntryTypeName>() << "rt.name" <<
                        LibPg::nullable_item<LogEntry::RawRequest>() << "rd_req.content" <<
                        LibPg::nullable_item<LogEntry::RawResponse>() << "rd_res.content" <<
                        LibPg::nullable_item<LogEntry::UserName>() << "r.user_name" <<
                        LibPg::nullable_item<LogEntry::UserId>() << "r.user_id" <<
                        LibPg::nullable_item<LogEntry::SessionId>() << "session.id" <<
                        LibPg::nullable_item<LogEntry::SessionLoginTime>() << "session.login_date "
            "FROM request r "
            "JOIN service s ON s.id = r.service_id "
            "JOIN request_type rt ON rt.id = r.request_type_id "
            "LEFT JOIN result_code rc ON rc.id = r.result_code_id "
            "LEFT JOIN request_data rd_req ON rd_req.request_id = r.id AND NOT rd_req.is_response "
                "AND rd_req.request_monitoring = " << LibPg::parameter<LogEntry::IsMonitoring>().as_boolean() << " "
                "AND " << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " <= rd_req.request_time_begin "
                "AND rd_req.request_time_begin < (" << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " + 1) "
                "AND rd_req.request_service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer() << " "
            "LEFT JOIN request_data rd_res ON rd_res.request_id = r.id AND rd_res.is_response "
                "AND rd_res.request_monitoring = " << LibPg::parameter<LogEntry::IsMonitoring>().as_boolean() << " "
                "AND " << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " <= rd_res.request_time_begin "
                "AND rd_res.request_time_begin < (" << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " + 1) "
                "AND rd_res.request_service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer() << " "
            "LEFT JOIN session ON session.id = r.session_id " // specific session partition would be better
            "WHERE r.is_monitoring = " << LibPg::parameter<LogEntry::IsMonitoring>().as_boolean() << " "
                "AND " << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " <= r.time_begin "
                "AND r.time_begin < (" << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " + 1) "
                "AND r.service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer() << " "
                "AND r.id = " << LibPg::parameter<LogEntry::LogEntryId>().as_big_int();
            // clang-format on

    const auto db_result = exec(
            _transaction,
            query,
            {
                _log_entry_ident.id,
                _log_entry_ident.time_begin,
                _service_id,
                _log_entry_ident.monitoring_status
            });

    if (db_result.empty())
    {
        throw LogEntryDoesNotExist{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results"};
    }

    const auto row = db_result.front();

    auto log_entry_info = LogEntryInfo{
            LogEntryIdent{
                    _log_entry_ident.id,
                    row.get<LogEntry::TimeBegin>(),
                    row.get<LogEntry::ServiceName>(),
                    _log_entry_ident.monitoring_status
            },
            row.get<LogEntry::TimeBegin>(),
            get_optional<LogEntry::TimeEnd>(row),
            get_optional<LogEntry::SourceIp>(row),
            get_optional<LogEntry::LogEntryTypeName>(row),
            get_optional<LogEntry::ResultCode>(row),
            get_optional<LogEntry::ResultName>(row),
            get_optional<LogEntry::RawRequest>(row),
            get_optional<LogEntry::RawResponse>(row),
            get_optional<LogEntry::UserName>(row),
            get_optional<LogEntry::UserId>(row),
            {},
            {},
            {},
            row.is_null<LogEntry::SessionId>()
                ? boost::optional<SessionIdent>{boost::none}
                : SessionIdent{
                        row.get_nullable<LogEntry::SessionId>(),
                        row.get_nullable<LogEntry::SessionLoginTime>()
                }
    };

    return log_entry_info;
}

std::map<LogEntry::IsOutput, std::vector<LogEntryProperty>> get_properties(
        const LibPg::PgTransaction& _transaction,
        const LogEntryIdent& _log_entry_ident,
        const LogEntry::ServiceId& _service_id)
{
    LIBLOG_DEBUG(__func__);
    std::map<LogEntry::IsOutput, std::vector<LogEntryProperty>> log_entry_properties;

    using ChildPropertyType = LibStrong::Type<std::string, struct ChildPropertyTypeTag_>;
    using ChildPropertyValue = LibStrong::Type<std::string, struct ChildPropertyValueTag_>;

    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::PropertyId>() << "rpv.id" <<
                        LibPg::item<LogEntry::PropertyType>() << "rpn.name" <<
                        LibPg::item<LogEntry::PropertyValue>() << "rpv.value" <<
                        LibPg::item<LogEntry::IsOutput>() << "rpv.output" <<
                        LibPg::nullable_item<ChildPropertyType>() << "child_rpn.name" <<
                        LibPg::nullable_item<ChildPropertyValue>() << "child.value " <<
              "FROM request_property_value rpv "
              "JOIN request_property_name rpn ON rpn.id=rpv.property_name_id "
              "LEFT JOIN request_property_value child ON child.parent_id = rpv.id "
               "AND child.request_monitoring = " << LibPg::parameter<LogEntry::IsMonitoring>().as_boolean() << " "
               "AND child.request_service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_big_int() << " "
               "AND " << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " <= child.request_time_begin "
               "AND child.request_time_begin < (" << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " + 1) "
               "AND child.request_id = " << LibPg::parameter<LogEntry::LogEntryId>().as_big_int() << " "
              "LEFT JOIN request_property_name child_rpn ON child_rpn.id = child.property_name_id "
             "WHERE rpv.request_monitoring = " << LibPg::parameter<LogEntry::IsMonitoring>().as_boolean() << " "
               "AND rpv.request_service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_big_int() << " "
               "AND " << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " <= rpv.request_time_begin "
               "AND rpv.request_time_begin < (" << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " + 1) "
               "AND rpv.request_id = " << LibPg::parameter<LogEntry::LogEntryId>().as_big_int() << " "
               "AND rpv.parent_id IS NULL "
             "ORDER BY rpv.output, rpn.name, rpv.id, child_rpn.name";
            // clang-format on

    const auto db_result =
            exec(_transaction,
                    query,
                    {_log_entry_ident.id,
                            _log_entry_ident.time_begin,
                            _service_id,
                            _log_entry_ident.monitoring_status});

    LogEntryProperty property;
    LogEntryPropertyValue property_value_instance;
    LogEntry::PropertyId previous_property_id;
    LogEntry::IsOutput previous_property_is_output;
    auto first_run = true;
    for (const auto& row : db_result)
    {
        const auto property_id = row.get<LogEntry::PropertyId>();
        const auto property_type = row.get<LogEntry::PropertyType>();
        const auto property_value = row.get<LogEntry::PropertyValue>();
        const auto property_is_output = row.get<LogEntry::IsOutput>();

        const bool new_property_id = !first_run && property_id != previous_property_id;
        if (new_property_id)
        {
            property.property_values.push_back(property_value_instance);
            log_entry_properties[previous_property_is_output].push_back(property);
            property_value_instance.children.clear();
            property.property_values.clear();
        }

        property.property_type = property_type;
        property_value_instance.property_value = property_value;
        const bool is_child =
                !row.is_null<ChildPropertyType>() &&
                !row.is_null<ChildPropertyValue>();
        if (is_child)
        {
            property_value_instance.children[LogEntry::PropertyType{*row.get_nullable<ChildPropertyType>()}].push_back(LogEntry::PropertyValue{*row.get_nullable<ChildPropertyValue>()});
        }

        previous_property_id = property_id;
        previous_property_is_output = property_is_output;
        first_run = false;
    }
    if (!db_result.empty())
    {
        property.property_values.push_back(property_value_instance);
        log_entry_properties[previous_property_is_output].push_back(property);
    }

    return log_entry_properties;
}

std::vector<ObjectReferences> get_object_references(
        const LibPg::PgTransaction& _transaction,
        const LogEntryIdent& _log_entry_ident,
        const LogEntry::ServiceId& _service_id)
{
    LIBLOG_DEBUG(__func__);
    std::vector<ObjectReferences> object_references;

    if (_log_entry_ident.time_begin->is_special())
    {
        throw InvalidLogEntryIdent{};
    }

    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::LogEntryObjectTypeName>() << "rot.name" <<
                        LibPg::item<LogEntry::ObjectIdent>() << "COALESCE(ror.object_ident::TEXT, ror.object_id::TEXT) "
              "FROM request_object_ref ror "
              "LEFT JOIN request_object_type rot ON rot.id=ror.object_type_id "
             "WHERE ror.request_monitoring = " << LibPg::parameter<LogEntry::IsMonitoring>().as_boolean() << " "
               "AND ror.request_service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_big_int() << " "
               "AND " << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " <= ror.request_time_begin "
               "AND ror.request_time_begin < (" << LibPg::parameter<LogEntry::TimeBeginDate>().as_date() << " + 1) "
               "AND ror.request_id = " << LibPg::parameter<LogEntry::LogEntryId>().as_big_int() << " "
             "ORDER BY rot.name, ror.id";
            // clang-format on

    const auto db_result =
            exec(_transaction,
                    query,
                    {_log_entry_ident.id,
                            _log_entry_ident.time_begin,
                            _service_id,
                            _log_entry_ident.monitoring_status});

    LogEntry::LogEntryObjectTypeName previous_type;
    ObjectReferences refs;

    auto first_run = true;
    for (const auto& row : db_result)
    {
        const auto object_type = row.get<LogEntry::LogEntryObjectTypeName>();
        const auto object_ident = row.get<LogEntry::ObjectIdent>();

        const bool new_object_type = !first_run && object_type != previous_type;
        if (new_object_type)
        {
            if (!refs.object_reference_ids.empty())
            {
                object_references.push_back(refs);
                refs.object_reference_ids.clear();
            }
        }
        refs.object_type = object_type;
        refs.object_reference_ids.push_back(object_ident);

        previous_type = object_type;
        first_run = false;
    }
    if (!refs.object_reference_ids.empty())
    {
        object_references.push_back(refs);
    }

    return object_references;
}

} // namespace Fred::LibLogger::{anonymous}

GetLogEntryInfoReply get_log_entry_info(
        const LibPg::PgTransaction& _transaction,
        const LogEntryIdent& _log_entry_ident)
{

    const auto service_id =
            get_service_id_by_name(_transaction, _log_entry_ident.service_name);

    LogEntryInfo log_entry_info = get_basic_info(_transaction, _log_entry_ident, service_id);
    {
        auto properties = get_properties(_transaction, _log_entry_ident, service_id);
        log_entry_info.input_properties = std::move(properties[LogEntry::IsOutput{false}]);
        log_entry_info.output_properties = std::move(properties[LogEntry::IsOutput{true}]);
    }
    log_entry_info.object_references = get_object_references(_transaction, _log_entry_ident, service_id);

    return GetLogEntryInfoReply{log_entry_info};
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util.hh"

#include "include/liblogger/exceptions.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

#include "liblog/liblog.hh"

#include <boost/asio/ip/address.hpp>

#include <fstream>
#include <stdexcept>

namespace Fred {
namespace LibLogger {

bool is_valid(const LogEntry::ServiceName& _service_name)
{
    return !_service_name->empty();
}

LogEntry::ServiceId get_service_id_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::ServiceName& _service_name)
{
    if (!is_valid(_service_name))
    {
        LIBLOG_DEBUG("invalid service \"{}\"", *_service_name);
        throw InvalidService{};
    }

    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::ServiceId>() << "id "
              "FROM service "
             "WHERE LOWER(name) = LOWER(REGEXP_REPLACE(" << LibPg::parameter<LogEntry::ServiceName>().as_text() << ", "
                                                      "'\\s', '', 'g'))";
            // clang-format on

    const auto db_result = exec(_transaction, query, _service_name);

    if (db_result.empty())
    {
        LIBLOG_DEBUG("service \"{}\" not found", *_service_name);
        throw ServiceNotFound{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    const auto service_id = db_result.front().get<LogEntry::ServiceId>();

    return service_id;
}

LogEntry::LogEntryObjectTypeId get_log_entry_object_type_id_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::LogEntryObjectTypeName& _log_entry_object_type_name)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::LogEntryObjectTypeId>() << "id "
              "FROM request_object_type "
             "WHERE name = " << LibPg::parameter<LogEntry::LogEntryObjectTypeName>().as_text();
            // clang-format on

    const auto db_result = exec(_transaction, query, _log_entry_object_type_name);

    if (db_result.empty())
    {
        LIBLOG_DEBUG("object type \"{}\" not found", *_log_entry_object_type_name);
        throw ObjectTypeNotFound{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    const auto log_entry_object_type_id = db_result.front().get<LogEntry::LogEntryObjectTypeId>();

    return log_entry_object_type_id;
}

LogEntry::LogEntryTypeId get_log_entry_type_id_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::LogEntryTypeName& _log_entry_type_name, const LogEntry::ServiceId& _service_id)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::LogEntryTypeId>() << "id "
              "FROM request_type "
             "WHERE name = " << LibPg::parameter<LogEntry::LogEntryTypeName>().as_text() << " "
               "AND service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer();
            // clang-format on

    const auto db_result = exec(_transaction, query, {_log_entry_type_name, _service_id});

    if (db_result.empty())
    {
        LIBLOG_DEBUG("log entry type \"{}\" of service with id \"{}\" not found", *_log_entry_type_name, *_service_id);
        throw LogEntryTypeNotFound{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    const auto log_entry_type_id = db_result.front().get<LogEntry::LogEntryTypeId>();

    return log_entry_type_id;
}

namespace {

LogEntry::PropertyNameId insert_log_entry_property(const LibPg::PgRwTransaction& _rw_transaction, const LogEntry::PropertyType& _property_type)
{
    const auto query = LibPg::make_query() <<
                 // clang-format off
                 "INSERT INTO request_property_name (name) "
                 "VALUES (" << LibPg::parameter<LogEntry::PropertyType>().as_text() << ") "
                     "ON CONFLICT (name) DO UPDATE SET name = EXCLUDED.name "
              "RETURNING" << LibPg::item<LogEntry::PropertyNameId>() << "id";
                 // clang-format on
    try
    {
        const auto db_result = exec(_rw_transaction, query, _property_type);

        const auto property_name_id = db_result.front().get<LogEntry::PropertyNameId>();

        LIBLOG_DEBUG("created new log_entry_property_name with id: \"{}\"", *property_name_id);

        return property_name_id;
    }
    catch (const LibPg::ExecFailure& e)
    {
        if (e.error_code() == LibPg::SqlState::string_data_right_truncation)
        {
            // the only string that can be truncated from text to varchar(256) is name
            throw InvalidPropertyType{};
        }
        throw;
    }
}

} // namespace Fred::LibLogger::{anonymous}

LogEntry::PropertyNameId get_log_entry_property_id_by_name(const LibPg::PgRwTransaction& _transaction, const LogEntry::PropertyType& _property_type)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::PropertyNameId>() << "id "
              "FROM request_property_name "
             "WHERE name = " << LibPg::parameter<LogEntry::PropertyType>().as_text();
            // clang-format on

    const auto db_result = exec(_transaction, query, _property_type);

    if (db_result.empty())
    {
        LIBLOG_DEBUG("log entry property \"{}\" not found, inserting as new", *_property_type);
        return insert_log_entry_property(_transaction, _property_type);
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    const auto property_name_id = db_result.front().get<LogEntry::PropertyNameId>();

    return property_name_id;
}

LogEntry::ResultCode get_result_code_by_name(const LibPg::PgTransaction& _transaction, const LogEntry::ResultName& _result_name, const LogEntry::ServiceId& _service_id)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<LogEntry::ResultCode>() << "result_code "
              "FROM result_code "
             "WHERE name = " << LibPg::parameter<LogEntry::ResultName>().as_text() << " "
               "AND service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer();
            // clang-format on

    const auto db_result = exec(_transaction, query, {_result_name, _service_id});

    if (db_result.empty())
    {
        LIBLOG_DEBUG("result code \"{}\" of service \"{}\" not found", *_result_name, *_service_id);
        throw ResultCodeNotFound{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results from database"};
    }

    const auto result_code = db_result.front().get<LogEntry::ResultCode>();

    return result_code;
}

} // namespace Fred::LibLogger
} // namespace Fred

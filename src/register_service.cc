/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/register_service.hh"

#include "include/liblogger/exceptions.hh"

#include "src/util.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

#include "libstrong/type.hh"

#include <regex>

namespace Fred {
namespace LibLogger {

namespace {

using PartitionPostfix = LibStrong::TypeWithSkills<std::string, struct PartitionPostfixTag_, LibStrong::Skill::Comparable>;

PartitionPostfix make_partition_postfix_from_service_handle(const LogEntry::ServiceHandle& _service_handle)
{
    return PartitionPostfix{*_service_handle + "_"};
}

bool is_valid(const LogEntry::ServiceHandle& _service_handle)
{
    const std::regex service_handle_regex{"[a-z0-9_]{1,9}"};
    return std::regex_match(*_service_handle, service_handle_regex);
}

void check_create_service_conflict(
        const LibPg::PgTransaction& _transaction,
        const LogEntry::ServiceName& _service_name,
        const PartitionPostfix& _service_partition_postfix)
{
    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<bool>() <<
                   "LOWER(name) = LOWER(" << LibPg::parameter<LogEntry::ServiceName>().as_text() << ") AND "
                   "LOWER(partition_postfix) = LOWER(" << LibPg::parameter<PartitionPostfix>().as_text() << ") "
              "FROM service "
             "WHERE LOWER(name) = LOWER(" << LibPg::parameter<LogEntry::ServiceName>().as_text() << ") OR "
                   "LOWER(partition_postfix) = LOWER(" << LibPg::parameter<PartitionPostfix>().as_text() << ")";
            // clang-format on

    const auto db_result = exec(_transaction, query, _service_name, _service_partition_postfix);

    if (db_result.size() == LibPg::RowIndex{1})
    {
        const auto service_registered = db_result.front().get<bool>();
        if (service_registered)
        {
            throw ServiceExists{};
        }
    }
    if (!db_result.empty())
    {
        throw InvalidService{};
    }
    throw ServiceRegistrationFailed{};
}

} // namespace Fred::LibLogger::{anonymous}

void register_service(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::ServiceName& _service_name,
        const LogEntry::ServiceHandle& _service_handle)
{
    if (!is_valid(_service_name) ||
        !is_valid(_service_handle))
    {
        throw InvalidService{};
    }

    const auto query = LibPg::make_query() <<
            // clang-format off
             "INSERT "
               "INTO service (partition_postfix, "
                             "name) "
             "SELECT " << LibPg::parameter<PartitionPostfix>().as_text() << ", " <<
                          LibPg::parameter<LogEntry::ServiceName>().as_text() << " "
              "WHERE NOT EXISTS (SELECT "
                                  "FROM service "
                                 "WHERE LOWER(name) = LOWER(" << LibPg::parameter<LogEntry::ServiceName>().as_text() << ") "
                                    "OR LOWER(partition_postfix) = LOWER(" << LibPg::parameter<PartitionPostfix>().as_text() << ")) "
                 "ON CONFLICT DO NOTHING";
            // clang-format on

    const auto service_partition_postfix = make_partition_postfix_from_service_handle(_service_handle);

    const LibPg::PgResultNoData db_result = [&]()
    {
        try
        {
            return exec(_rw_transaction, query, {service_partition_postfix, _service_name});
        }
        catch (const LibPg::ExecFailure& e)
        {
            if (e.error_code() == LibPg::SqlState::string_data_right_truncation)
            {
                throw InvalidService{};
            }
            throw;
        }
    }();
    if (db_result.get_number_of_rows_affected() == LibPg::RowIndex{0})
    {
        check_create_service_conflict(_rw_transaction, _service_name, service_partition_postfix);
    }
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/log_entry.hh"

#include "src/log_entry.hh"
#include "libpg/query.hh"

#include <boost/optional.hpp>

#include <cstdlib>

namespace Fred {
namespace LibLogger {

namespace LogEntry {

void insert_log_entry_data(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const Content& _content,
        const IsResponse& _is_response)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "INSERT INTO request_data ("
               "request_time_begin, "
               "request_service_id, "
               "request_monitoring, "
               "request_id, "
               "content, "
               "is_response) "
            "VALUES (" <<
                LibPg::parameter<TimeBegin>().as_timestamp() << ", " <<
                LibPg::parameter<ServiceId>().as_big_int() << ", " <<
                LibPg::parameter<IsMonitoring>().as_boolean() << ", " <<
                LibPg::parameter<LogEntryId>().as_big_int() << ", " <<
                LibPg::parameter<Content>().as_text() << ", " <<
                LibPg::parameter<IsResponse>().as_boolean() << ")";
            // clang-format on

    LibPg::PgResultNoData{
            exec(_rw_transaction, query,
                {_time_begin,
                _service_id,
                _is_monitoring,
                _log_entry_id,
                _content,
                _is_response})};
}

PropertyId insert_log_entry_property(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const PropertyNameId& _property_name_id,
        const PropertyValue& _property_value,
        const IsOutput& _is_output,
        const boost::optional<PropertyId>& _parent_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "INSERT INTO request_property_value ("
               "request_time_begin, "
               "request_service_id, "
               "request_monitoring, "
               "request_id, "
               "property_name_id, "
               "value, "
               "output, "
               "parent_id) "
            "VALUES (" <<
                LibPg::parameter<TimeBegin>().as_timestamp() << ", " <<
                LibPg::parameter<ServiceId>().as_big_int() << ", " <<
                LibPg::parameter<IsMonitoring>().as_boolean() << ", " <<
                LibPg::parameter<LogEntryId>().as_big_int() << ", " <<
                LibPg::parameter<PropertyNameId>().as_integer() << ", " <<
                LibPg::parameter<PropertyValue>().as_text() << ", " <<
                LibPg::parameter<IsOutput>().as_boolean() << ", " <<
                LibPg::parameter<boost::optional<PropertyId>>().as_big_int() << ")";
            // clang-format on

    LibPg::PgResultNoData{
            exec(_rw_transaction, query,
                {_time_begin,
                _service_id,
                _is_monitoring,
                _log_entry_id,
                _property_name_id,
                _property_value,
                _is_output,
                _parent_id})};

    // get the id of the above INSERT command using SELECT CURRVAL, because there is a trigger on insert to request_property_value which does not support RETURNING clause
    const auto db_result =
            exec(_rw_transaction, LibPg::make_query() << "SELECT" << LibPg::item<PropertyId>() << "CURRVAL('request_property_value_id_seq'::REGCLASS)");

    auto property_id = db_result.front().get<PropertyId>();

    return property_id;
}

PropertyId insert_log_entry_property_as_parent(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const PropertyNameId& _property_name_id,
        const PropertyValue& _property_value,
        const IsOutput& _is_output)
{
    const auto no_parent_id = boost::none;
    return insert_log_entry_property(
            _rw_transaction,
            _time_begin,
            _service_id,
            _is_monitoring,
            _log_entry_id,
            _property_name_id,
            _property_value,
            _is_output,
            no_parent_id);
}

void insert_log_entry_property_as_child(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const PropertyNameId& _property_name_id,
        const PropertyValue& _property_value,
        const IsOutput& _is_output,
        const PropertyId& _parent_id)
{
    insert_log_entry_property(
            _rw_transaction,
            _time_begin,
            _service_id,
            _is_monitoring,
            _log_entry_id,
            _property_name_id,
            _property_value,
            _is_output,
            _parent_id);
}

void insert_object_reference(
        const LibPg::PgRwTransaction& _rw_transaction,
        const TimeBegin& _time_begin,
        const ServiceId& _service_id,
        const IsMonitoring& _is_monitoring,
        const LogEntryId& _log_entry_id,
        const LogEntryObjectTypeId& _log_entry_object_type_id,
        const ObjectIdent& _object_ident)
{
    const auto object_ident_variant = get_ident_variants_from(_object_ident);

    const auto query = LibPg::make_query() <<
            // clang-format off
            "INSERT INTO request_object_ref ("
               "request_time_begin, "
               "request_service_id, "
               "request_monitoring, "
               "request_id, "
               "object_type_id, "
               "object_id, "
               "object_ident) "
            "VALUES (" <<
                LibPg::parameter<TimeBegin>().as_timestamp() << ", " <<
                LibPg::parameter<ServiceId>().as_big_int() << ", " <<
                LibPg::parameter<IsMonitoring>().as_boolean() << ", " <<
                LibPg::parameter<LogEntryId>().as_big_int() << ", " <<
                LibPg::parameter<LogEntryObjectTypeId>().as_integer() << ", " <<
                LibPg::parameter<ObjectId>().as_integer() << ", " <<
                LibPg::parameter<boost::optional<ObjectIdent>>().as_text() << ")";
            // clang-format on

    LibPg::PgResultNoData{
            exec(_rw_transaction, query,
                {_time_begin,
                _service_id,
                _is_monitoring,
                _log_entry_id,
                _log_entry_object_type_id,
                std::get<ObjectId>(object_ident_variant),
                std::get<boost::optional<ObjectIdent>>(object_ident_variant)})};
}

std::tuple<ObjectId, boost::optional<ObjectIdent>, CorbaImplCompatibility> get_ident_variants_from(const ObjectIdent& _object_ident)
{
    const auto numeric_id = std::atoi(_object_ident->c_str()); // std::atoi does not throw, but you must check the result
    const bool is_numeric = std::to_string(numeric_id) == *_object_ident; // check by the inverse transformation
    if (is_numeric)
    {
        // for old corba implementation, if object_ident is convertible to int, use it as object_id, not object_ident
        return make_tuple(ObjectId{numeric_id}, boost::optional<ObjectIdent>{}, CorbaImplCompatibility{is_numeric});
    }
    // for new grpc implementation, object_ident is NOT convertible to int, use it as is, and expect object_id == 0
    // (no object id is represented as 0 for compatibility with old corba implementation)
    return make_tuple(ObjectId{}, boost::optional<ObjectIdent>{_object_ident}, CorbaImplCompatibility{is_numeric});
}

} // namespace Fred::LibLogger::LogEntry

LogEntryPropertyValue::LogEntryPropertyValue(
        LogEntry::PropertyValue _property_value,
        std::map<LogEntry::PropertyType, std::vector<LogEntry::PropertyValue>> _children)
    : property_value(std::move(_property_value)),
      children(std::move(_children))
{
}

LogEntryProperty::LogEntryProperty(
        LogEntry::PropertyType _property_type,
        std::vector<LogEntryPropertyValue> _property_values)
    : property_type(std::move(_property_type)),
      property_values(std::move(_property_values))
{
}

ObjectReferences::ObjectReferences(
        LogEntry::LogEntryObjectTypeName _object_type,
        std::vector<LogEntry::ObjectIdent> _object_reference_ids)
    : object_type(std::move(_object_type)),
      object_reference_ids(std::move(_object_reference_ids))
{
}

} // namespace Fred::LibLogger
} // namespace Fred

namespace boost {
namespace asio {
namespace ip {

std::string wrap_into_psql_representation(const boost::asio::ip::address& value)
{
    try
    {
        return value.to_string();
    }
    catch (...)
    {
        struct UnexpectedValue : Fred::LibLogger::WrapperError
        {
            const char* what() const noexcept override
            {
                return "unexpected value of boost::asio::ip::address";
            }
        };
        throw UnexpectedValue{};
    }
}

boost::asio::ip::address unwrap_from_psql_representation(const boost::asio::ip::address&, const char* src)
{
    if (src == nullptr)
    {
        struct MissingValue : Fred::LibLogger::UnwrapperError
        {
            const char* what()const noexcept override { return "value is NULL"; }
        };
        throw MissingValue{};
    }
    try
    {
        return boost::asio::ip::address::from_string(src);
    }
    catch (...)
    {
        struct InvalidValue : Fred::LibLogger::UnwrapperError
        {
            const char* what()const noexcept override { return "unable to convert to boost::asio::ip:address value"; }
        };
        throw InvalidValue{};
    }
}

} // namespace boost::asio::ip
} // namespace boost::asio
} // namespace boost

/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/register_log_entry_types.hh"

#include "include/liblogger/exceptions.hh"

#include "src/util.hh"

#include "libpg/query.hh"
#include "libpg/sql_state/code.hh"

namespace Fred {
namespace LibLogger {

namespace {

void insert_log_entry_type(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::LogEntryTypeName& _log_entry_type,
        const LogEntry::ServiceId& _service_id)
{
    if (!is_valid(_log_entry_type))
    {
        throw InvalidLogEntryType{};
    }

    const auto query = LibPg::make_query() <<
            // clang-format off
             "INSERT "
               "INTO request_type (name, "
                                  "service_id) "
             "SELECT " << LibPg::parameter<LogEntry::LogEntryTypeName>().as_text() << ", " <<
                          LibPg::parameter<LogEntry::ServiceId>().as_integer() << " "
              "WHERE NOT EXISTS (SELECT "
                                  "FROM request_type "
                                 "WHERE LOWER(name) = LOWER(" << LibPg::parameter<LogEntry::LogEntryTypeName>().as_text() << ") "
                                   "AND service_id = " << LibPg::parameter<LogEntry::ServiceId>().as_integer() << ") "
                 "ON CONFLICT DO NOTHING";
            // clang-format on

    try
    {
        LibPg::PgResultNoData{
            exec(_rw_transaction, query, {_log_entry_type, _service_id})};
    }
    catch (const LibPg::ExecFailure& e)
    {
        if (e.error_code() == LibPg::SqlState::string_data_right_truncation)
        {
            // the only string that can be truncated from text to varchar(64) is _log_entry_type
            throw InvalidLogEntryType{};
        }
        throw;
    }

}

} // namespace Fred::LibLogger::{anonymous}

bool is_valid(const LogEntry::LogEntryTypeName& _log_entry_type)
{
    return !_log_entry_type->empty();
}

void register_log_entry_types(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::ServiceName& _service,
        const std::vector<LogEntry::LogEntryTypeName>& _log_entry_types)
{
    const auto service_id = get_service_id_by_name(_rw_transaction, _service);

    for (const auto& log_entry_type : _log_entry_types)
    {
        insert_log_entry_type(_rw_transaction, log_entry_type, service_id);
    }
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/close_session.hh"

#include "include/liblogger/exceptions.hh"

#include "liblog/liblog.hh"

#include "libpg/query.hh"

namespace Fred {
namespace LibLogger {

void close_session(
        const LibPg::PgRwTransaction& _rw_transaction,
        const SessionIdent& _session_ident)
{
    LIBLOG_INFO("closing session with session_id: \"{}\"", *_session_ident.id);

    const auto query =
            LibPg::make_query() <<
            // clang-format off
            "UPDATE session " << 
               "SET logout_date = CURRENT_TIMESTAMP "
             "WHERE id = " << LibPg::parameter<LogEntry::SessionId>().as_big_int() << " AND " <<
                    LibPg::parameter<LogEntry::SessionLoginDate>().as_date() << " <= login_date AND "
                   "login_date < (" << LibPg::parameter<LogEntry::SessionLoginDate>().as_date() << " + 1) AND "
                   "logout_date IS NULL";
            // clang-format on

    const auto db_result =
            exec(_rw_transaction, query, {_session_ident.id, _session_ident.login_date});

    static_assert(std::is_same<std::decay_t<decltype(db_result)>, LibPg::PgResultNoData>::value, "result must be of LibPg::PgResultNoData type");

    if (db_result.get_number_of_rows_affected() == LibPg::RowIndex{1})
    {
        return;
    }

    if (db_result.get_number_of_rows_affected() > LibPg::RowIndex{1})
    {
        throw std::runtime_error("too many rows affected by update");
    }

    if (db_result.get_number_of_rows_affected() == LibPg::RowIndex{0})
    {
        const auto query =
                LibPg::make_query() <<
                // clang-format off
                "SELECT" << LibPg::item<bool>() << "logout_date IS NULL "
                  "FROM session "
                 "WHERE id = " << LibPg::parameter<LogEntry::SessionId>().as_big_int() << " AND " <<
                        LibPg::parameter<LogEntry::SessionLoginDate>().as_date() << " <= login_date AND "
                       "login_date < (" << LibPg::parameter<LogEntry::SessionLoginDate>().as_date() << " + 1)";
                // clang-format on

        const auto db_result =
                exec(_rw_transaction, query, {_session_ident.id, _session_ident.login_date});

        if (db_result.empty())
        {
            throw SessionDoesNotExist{};
        }

        if (db_result.size() != LibPg::RowIndex{1})
        {
            throw std::runtime_error{"unexpected number of results"};
        }

        const auto row = db_result.front();
        if (!row.get<bool>())
        {
            throw SessionAlreadyClosed{};
        }
    }
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_OPTIONAL_HH_313C75907A8A4C81AB368D3A40D79EE5
#define GET_OPTIONAL_HH_313C75907A8A4C81AB368D3A40D79EE5

#include "include/liblogger/log_entry.hh"

#include "libpg/named_columns.hh"

#include <boost/optional.hpp>

#include <utility>

namespace Fred {
namespace LibLogger {

template <typename T>
struct MakeOptional
{
    using Optional = boost::optional<T>;
    decltype(auto) operator()() const
    {
        return Optional{};
    }
    template <typename Src>
    decltype(auto) operator()(Src&& src) const
    {
        return Optional{std::forward<Src>(src)};
    }
};

template <typename T, typename Tag, template <typename> class Skills>
struct MakeOptional<LibStrong::Type<T, Tag, Skills>>
{
    using Optional = LibStrong::Optional<LibStrong::Type<T, Tag, Skills>>;
    decltype(auto) operator()() const
    {
        return Optional{};
    }
    template <typename Src>
    decltype(auto) operator()(Src&& src) const
    {
        return Optional{std::forward<Src>(src)};
    }
};

template <typename Item, typename... Types>
decltype(auto) get_optional(const LibPg::NamedColumns<Types...>& row)
{
    static constexpr auto make_optional = MakeOptional<Item>{};
    if (row.template is_null<Item>())
    {
        return make_optional();
    }
    return make_optional(row.template get_nullable<Item>());
}

} // namespace Fred::LibLogger
} // namespace Fred

#endif

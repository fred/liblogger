/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/get_session_user.hh"
#include "src/get_optional.hh"

#include "include/liblogger/exceptions.hh"

#include "libpg/query.hh"

namespace Fred {
namespace LibLogger {

SessionUser get_session_user(const LibPg::PgTransaction& _transaction, const SessionIdent& _session_ident)
{
    if (_session_ident.login_date->is_special())
    {
        throw SessionDoesNotExist{};
    }

    const auto query =
            // clang-format off
            LibPg::make_query() <<
            "SELECT" << LibPg::item<LogEntry::UserName>() << "user_name" <<
                        LibPg::nullable_item<LogEntry::UserId>() << "user_id "
              "FROM session "
             "WHERE id = " << LibPg::parameter<LogEntry::SessionId>().as_big_int() << " AND " <<
                    LibPg::parameter<LogEntry::SessionLoginDate>().as_date() << " <= login_date AND "
                   "login_date < (" << LibPg::parameter<LogEntry::SessionLoginDate>().as_date() << " + 1)";
            // clang-format on

    const auto db_result = exec(_transaction, query, {_session_ident.id, _session_ident.login_date});

    if (db_result.empty())
    {
        throw SessionDoesNotExist{};
    }

    if (db_result.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of results"};
    }

    const auto row = db_result.front();
    const auto user_name = row.get<LogEntry::UserName>();
    const auto user_id = get_optional<LogEntry::UserId>(row);

    return SessionUser{user_name, user_id};
}

} // namespace Fred::LibLogger
} // namespace Fred

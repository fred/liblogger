/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/close_log_entry.hh"

#include "include/liblogger/exceptions.hh"
#include "src/get_session_user.hh"
#include "src/log_entry.hh"
#include "src/util.hh"

#include "libpg/extendable_query_arguments.hh"
#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"

#include "liblog/liblog.hh"

#include <boost/format.hpp>

#include <stdexcept>

namespace Fred {
namespace LibLogger {

namespace {

auto update_log_entry(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntryIdent& _log_entry_ident,
        const LibStrong::Optional<LogEntry::UserName>& _session_user_name,
        const LibStrong::Optional<LogEntry::UserId>& _session_user_id,
        const LogEntry::ServiceId& _service_id,
        const LogEntry::ResultCode& _result_code,
        const LibStrong::Optional<LogEntry::SessionId>& _session_id)
{
    LibPg::ExtendableQueryArguments query_arguments{
            _service_id,
            _result_code,
            _log_entry_ident.id,
            _log_entry_ident.time_begin,
            _log_entry_ident.monitoring_status};

    if (_session_id != LogEntry::SessionId::nullopt)
    {
        query_arguments.add(*_session_id);
    }

    std::string query =
            // clang-format off
            "UPDATE request "
               "SET time_end = CURRENT_TIMESTAMP, ";
    if (_session_user_name != LogEntry::UserName::nullopt)
    {
          // user_name.as_text() should fit into VARCHAR(255), because it was taken from session table as VARCHAR(255)
          query += "user_name = " + query_arguments.add(*_session_user_name).as_text() + ", ";
    }
    if (_session_user_id != LogEntry::UserId::nullopt)
    {
          query += "user_id = " + query_arguments.add(*_session_user_id).as_integer() + ", ";
    }
    if (_session_id != LogEntry::SessionId::nullopt)
    {
          query += "session_id = " + query_arguments.parameter(5).as_big_int() + ", ";
    }
    query += "result_code_id = get_result_code_id(" + query_arguments.parameter(0).as_integer() + ", " + query_arguments.parameter(1).as_integer() + ") "
            "WHERE id = " + query_arguments.parameter(2).as_big_int() + " "
              "AND " + query_arguments.parameter(3).as_date() + "<= time_begin "
              "AND time_begin < (" + query_arguments.parameter(3).as_date() + " + 1) "
              "AND service_id = " + query_arguments.parameter(0).as_integer() + " ";
    if (_session_id != LogEntry::SessionId::nullopt)
    {
          query += "AND (session_id = " + query_arguments.parameter(5).as_big_int() + " OR session_id IS NULL) ";
    }
    query +=  "AND is_monitoring = " + query_arguments.parameter(4).as_boolean() + " "
              "AND time_end IS NULL "
            "RETURNING time_begin";
            // clang-format on

    const LibPg::PgResultTuples db_result = exec(_rw_transaction, query, query_arguments);
    //const auto db_result = exec(_rw_transaction, query, query_arguments);
    //static_assert(std::is_same<std::decay_t<decltype(db_result)>, LibPg::PgResultNoData>::value, "result must be of LibPg::PgResultNoData type");

    if (db_result.empty())
    {
        throw LogEntryDoesNotExist{};
    }
    if (db_result.get_number_of_rows_affected() > LibPg::RowIndex{1})
    {
        throw std::runtime_error("too many rows affected by update");
    }
    return db_result[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<LogEntry::TimeBegin>();
}

class GetResultCode : public boost::static_visitor<LogEntry::ResultCode>
{
public:
    GetResultCode(const LibPg::PgTransaction& _transaction, const LogEntry::ServiceId& _service_id)
        : transaction_{_transaction},
          service_id_{_service_id}
    {
    }

    LogEntry::ResultCode operator()(const LogEntry::ResultCode& _result_code) const
    {
        return _result_code;
    }

    LogEntry::ResultCode operator()(const LogEntry::ResultName& _result_name) const
    {
        return get_result_code_by_name(transaction_, _result_name, service_id_);
    }

    const LibPg::PgTransaction& transaction_;
    const LogEntry::ServiceId& service_id_;
};

LogEntry::ResultCode get_result_code_by_code_or_name(
        const LibPg::PgTransaction& _transaction,
        const boost::variant<LogEntry::ResultCode, LogEntry::ResultName>& _result_code_or_name,
        const LogEntry::ServiceId& _service_id)
{
    return boost::apply_visitor(GetResultCode{_transaction, _service_id}, _result_code_or_name);
}

} // namespace Fred::LibLogger::{anonymous}

void close_log_entry(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntryIdent& _log_entry_ident,
        const LibStrong::Optional<LogEntry::Content>& _log_entry_content,
        const std::vector<LogEntryProperty>& _log_entry_properties,
        const std::vector<ObjectReferences>& _object_references,
        const boost::variant<LogEntry::ResultCode, LogEntry::ResultName>& _result_code_or_name,
        const boost::optional<SessionIdent>& _session_ident)
{
    LibLog::SetContext ctx{
            boost::str(boost::format("session-%1%") % (_session_ident != boost::none ? std::to_string(*(_session_ident->id)) : "N/A")), // TODO: get session from db if not provided here?
            boost::str(boost::format("request-%1%") % _log_entry_ident.id)};

    const auto session_user =
            _session_ident != boost::none
                    ? boost::optional<SessionUser>{get_session_user(_rw_transaction, *_session_ident)}
                    : boost::none;

    const auto service_id =
            get_service_id_by_name(_rw_transaction, _log_entry_ident.service_name);

    const auto result_code =
            get_result_code_by_code_or_name(_rw_transaction, _result_code_or_name, service_id);

    const auto time_begin = update_log_entry(
            _rw_transaction,
            _log_entry_ident,
            {session_user != boost::none, [&]() { return session_user->name; }},
            session_user != boost::none ? session_user->id : LogEntry::UserId::nullopt,
            service_id,
            result_code,
            {_session_ident != boost::none, [&]() { return _session_ident->id; }});

    if (_log_entry_content != LogEntry::Content::nullopt && !(*_log_entry_content)->empty())
    {
        static constexpr auto is_response = LogEntry::IsResponse{true};
        LogEntry::insert_log_entry_data(
                _rw_transaction,
                time_begin,
                service_id,
                _log_entry_ident.monitoring_status,
                _log_entry_ident.id,
                *_log_entry_content,
                is_response);
    }

    for (const auto& log_entry_property : _log_entry_properties)
    {
        if (log_entry_property.property_values.empty())
        {
            LIBLOG_WARNING("unexpected number of property values: input contains 0 property values of type \"{}\"", *log_entry_property.property_type);
        }
        const auto property_name_id = get_log_entry_property_id_by_name(_rw_transaction, log_entry_property.property_type);
        for (const auto& property : log_entry_property.property_values)
        {
            static constexpr auto is_output = LogEntry::IsOutput{true};
            const auto inserted_property_id =
                    LogEntry::insert_log_entry_property_as_parent(
                            _rw_transaction,
                            time_begin,
                            service_id,
                            _log_entry_ident.monitoring_status,
                            _log_entry_ident.id,
                            property_name_id,
                            property.property_value,
                            is_output);

            for (const auto& child : property.children)
            {
                const auto child_property_type = child.first;
                for (const auto& child_property_value : child.second)
                {
                    const auto child_property_name_id = get_log_entry_property_id_by_name(_rw_transaction, child_property_type);
                    LogEntry::insert_log_entry_property_as_child(
                            _rw_transaction,
                            time_begin,
                            service_id,
                            _log_entry_ident.monitoring_status,
                            _log_entry_ident.id,
                            child_property_name_id,
                            child_property_value,
                            is_output,
                            inserted_property_id);
                }
            }
        }
    }

    for (const auto& object_reference : _object_references)
    {
        if (object_reference.object_reference_ids.empty())
        {
            LIBLOG_WARNING("unexpected number of object references: input contains 0 references of type \"{}\"", *object_reference.object_type);
        }
        for (const auto& object_reference_id : object_reference.object_reference_ids)
        {
            const auto log_entry_object_type_id = get_log_entry_object_type_id_by_name(_rw_transaction, object_reference.object_type);
            insert_object_reference(
                    _rw_transaction,
                    time_begin,
                    service_id,
                    _log_entry_ident.monitoring_status,
                    _log_entry_ident.id,
                    log_entry_object_type_id,
                    object_reference_id);
        }
    }
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/liblogger/exceptions.hh"

namespace Fred {
namespace LibLogger {

const char* SessionDoesNotExist::what() const noexcept
{
    return "session does not exist";
}

const char* SessionAlreadyClosed::what() const noexcept
{
    return "session already closed";
}

const char* InvalidService::what() const noexcept
{
    return "invalid service";
}

const char* ServiceNotFound::what() const noexcept
{
    return "service not found";
}

const char* ServiceExists::what() const noexcept
{
    return "service exists";
}

const char* ServiceRegistrationFailed::what() const noexcept
{
    return "service registration failed";
}

const char* InvalidTimestampFrom::what() const noexcept
{
    return "from is not a date";
}

const char* InvalidTimestampTo::what() const noexcept
{
    return "to is not a date";
}

const char* InvalidTimestampRange::what() const noexcept
{
    return "invalid range of dates";
}

const char* InvalidCountLimit::what() const noexcept
{
    return "count_limit has to be a positive number";
}

const char* ListLogEntriesException::what() const noexcept
{
    return "failed to list log entries due to an unknown exception";
}

const char* LogEntryDoesNotExist::what() const noexcept
{
    return "log entry does not exist";
}

const char* InvalidLogEntryIdent::what() const noexcept
{
    return "log entry ident has a bad format";
}

const char* InvalidLogEntryType::what() const noexcept
{
    return "invalid log entry type";
}

const char* InvalidSessionIdent::what() const noexcept
{
    return "session ident has a bad format";
}

const char* InvalidUserName::what() const noexcept
{
    return "invalid user name";
}

const char* LogEntryTypeNotFound::what() const noexcept
{
    return "log entry type not found";
}

const char* InvalidPropertyType::what() const noexcept
{
    return "invalid property type";
}

const char* InvalidObjectType::what() const noexcept
{
    return "invalid object type";
}

const char* ObjectTypeNotFound::what() const noexcept
{
    return "object type not found";
}

const char* InvalidResultName::what() const noexcept
{
    return "invalid result name";
}

const char* InvalidResult::what() const noexcept
{
    return "invalid result";
}

const char* ResultCodeNotFound::what() const noexcept
{
    return "result code not found";
}

} // namespace Fred::LibLogger
} // namespace Fred

/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/liblogger/session_ident.hh"

#include "include/liblogger/exceptions.hh"

namespace Fred {
namespace LibLogger {

namespace {

LogEntry::SessionLoginDate validate(const LogEntry::SessionLoginDate& _login_date)
{
    if (_login_date->is_special())
    {
        throw InvalidSessionIdent{};
    }
    return _login_date;
}

LogEntry::SessionLoginDate extract_date(const LogEntry::SessionLoginTime& time)
{
    return LogEntry::SessionLoginDate{time->date()};
}

} // namespace Fred::LibLogger::{anonymous}

SessionIdent::SessionIdent(
            const LogEntry::SessionId& _id,
            const LogEntry::SessionLoginDate& _login_date)
        : id(_id),
          login_date(validate(_login_date))
{ }

SessionIdent::SessionIdent(
        const LogEntry::SessionId& _id,
        const LogEntry::SessionLoginTime& _login_time)
    : SessionIdent{_id, extract_date(_login_time)}
{ }

} // namespace Fred::LibLogger
} // namespace Fred

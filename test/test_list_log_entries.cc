/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "fixture/list_log_entries.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/list_log_entries.hh"
#include "liblogger/log_entry_ident.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/test/unit_test.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <string>
#include <vector>

namespace Test {

namespace {

const std::vector<::Fred::LibLogger::LogEntry::LogEntryTypeName> log_entry_types{
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainCheck"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainInfo"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainDelete"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainUpdate"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainCreate"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainTransfer"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainRenew"},
        ::Fred::LibLogger::LogEntry::LogEntryTypeName{"DomainTrade"}};

bool operator>=(const ::Fred::LibLogger::LogEntry::TimeBegin& _lhs, const ::Fred::LibLogger::LogEntry::TimeEnd& _rhs)
{
    return _lhs.get() >= _rhs.get();
}

bool check_log_entries(
        const std::vector<::Fred::LibLogger::LogEntryIdent>& _log_entries,
        const ::Fred::LibLogger::LogEntry::TimeBegin _from,
        const ::Fred::LibLogger::LogEntry::TimeEnd _to,
        const ::Fred::LibLogger::LogEntry::ServiceName& _service)
{
    for (const auto& log_entry : _log_entries)
    {
        if (log_entry.id.get() < 1)
        {
            throw std::runtime_error("Invalid id of log entry.");
        }
        if (log_entry.time_begin < _from || log_entry.time_begin >= _to)
        {
            throw std::runtime_error("Time begin has invalid range.");
        }
        if (log_entry.service_name != _service)
        {
            throw std::runtime_error("Service name is " + log_entry.service_name.get() + " instead of " + _service.get());
        }
        if (log_entry.monitoring_status != ::Fred::LibLogger::LogEntry::IsMonitoring{false})
        {
            throw std::runtime_error("Monitoring status is not false.");
        }
    }
    return true;
}

}// namespace Test::{anonymous}

BOOST_AUTO_TEST_SUITE(TestListLogEntries)

BOOST_FIXTURE_TEST_CASE(service_not_found, Util::AddFixture<Util::SupplyCtx<Fixture::HasNoLogEntries>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    ::Fred::LibLogger::LogEntry::ServiceName{"nonexistent_service"},
                    fixture.time_begin,
                    fixture.time_end,
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(bad_from_date, Util::AddFixture<Util::SupplyCtx<Fixture::HasNoLogEntries>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    ::Fred::LibLogger::LogEntry::TimeBegin{},
                    fixture.time_end,
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types),
            ::Fred::LibLogger::InvalidTimestampFrom);
}

BOOST_FIXTURE_TEST_CASE(bad_to_date, Util::AddFixture<Util::SupplyCtx<Fixture::HasNoLogEntries>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    ::Fred::LibLogger::LogEntry::TimeEnd{},
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types),
            ::Fred::LibLogger::InvalidTimestampTo);
}

BOOST_FIXTURE_TEST_CASE(bad_date_range, Util::AddFixture<Util::SupplyCtx<Fixture::HasNoLogEntries>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    ::Fred::LibLogger::LogEntry::TimeEnd{fixture.time_begin.get()},
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types),
            ::Fred::LibLogger::InvalidTimestampRange);
}

BOOST_FIXTURE_TEST_CASE(bad_count_limit, Util::AddFixture<Util::SupplyCtx<Fixture::HasNoLogEntries>>)
{
    const unsigned int invalid_count_limit = 0;
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    fixture.time_end,
                    invalid_count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types),
            ::Fred::LibLogger::InvalidCountLimit);
}

BOOST_FIXTURE_TEST_CASE(test_list_log_entries_empty_response, Util::AddFixture<Util::SupplyCtx<Fixture::HasSeveralLogEntries>>)
{
    ::Fred::LibLogger::ListLogEntriesReply result;

    BOOST_CHECK_NO_THROW(
            result = ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    fixture.time_end,
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{"nonexistent_username"}, // gives empty response
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types));

    BOOST_CHECK(result.log_entry_ids.empty());
}

BOOST_FIXTURE_TEST_CASE(test_list_log_entries_empty_object_reference, Util::AddFixture<Util::SupplyCtx<Fixture::HasSeveralLogEntries>>)
{
    ::Fred::LibLogger::ListLogEntriesReply result;
    BOOST_CHECK_NO_THROW(
            result = ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    fixture.time_end,
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{
                            ::Fred::LibLogger::ObjectReference{
                                    ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType"},
                                    ::Fred::LibLogger::LogEntry::ObjectIdent{""}}},
                    fixture.log_entry_types));

    BOOST_CHECK(!result.log_entry_ids.empty());
    BOOST_CHECK(result.log_entry_ids.size() <= fixture.count_limit);
    BOOST_CHECK(check_log_entries(result.log_entry_ids, fixture.time_begin, fixture.time_end, fixture.service));
}

BOOST_FIXTURE_TEST_CASE(test_list_log_entries_count_limit, Util::AddFixture<Util::SupplyCtx<Fixture::HasSeveralLogEntries>>)
{
    BOOST_REQUIRE(fixture.count_limit > 2);
    const unsigned int count_limit = fixture.count_limit - 2;

    ::Fred::LibLogger::ListLogEntriesReply result;
    BOOST_CHECK_NO_THROW(
            result = ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    fixture.time_end,
                    count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types));

    BOOST_CHECK(result.log_entry_ids.size() == count_limit);
    BOOST_CHECK(check_log_entries(result.log_entry_ids, fixture.time_begin, fixture.time_end, fixture.service));
}

BOOST_FIXTURE_TEST_CASE(test_list_log_entries_min, Util::AddFixture<Util::SupplyCtx<Fixture::HasSeveralLogEntries>>)
{
    ::Fred::LibLogger::ListLogEntriesReply result;
    BOOST_CHECK_NO_THROW(
            result = ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    fixture.time_end,
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types));

    BOOST_CHECK(!result.log_entry_ids.empty());
    BOOST_CHECK(result.log_entry_ids.size() <= fixture.count_limit);
    BOOST_CHECK(check_log_entries(result.log_entry_ids, fixture.time_begin, fixture.time_end, fixture.service));
}

BOOST_FIXTURE_TEST_CASE(test_list_log_entries_max, Util::AddFixture<Util::SupplyCtx<Fixture::HasSeveralLogEntries>>)
{
    ::Fred::LibLogger::ListLogEntriesReply result;
    BOOST_CHECK_NO_THROW(
            result = ::Fred::LibLogger::list_log_entries(
                    fixture.transaction,
                    fixture.service,
                    fixture.time_begin,
                    fixture.time_end,
                    fixture.count_limit,
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    boost::optional<::Fred::LibLogger::ObjectReference>{},
                    fixture.log_entry_types));

    BOOST_CHECK(!result.log_entry_ids.empty());
    BOOST_CHECK(result.log_entry_ids.size() <= fixture.count_limit);
    BOOST_CHECK(check_log_entries(result.log_entry_ids, fixture.time_begin, fixture.time_end, fixture.service));
}

BOOST_AUTO_TEST_SUITE_END() // TestListLogEntries

} // namespace Test

/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/close_log_entry.hh"
#include "liblogger/exceptions.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestCloseLogEntry)

BOOST_FIXTURE_TEST_CASE(nonexistent_log_entry_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasCloseLogEntryDataOfNonexistentLogEntry>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::close_log_entry(
                    fixture.transaction,
                    fixture.close_log_entry_data.log_entry_ident,
                    fixture.close_log_entry_data.log_entry_content,
                    fixture.close_log_entry_data.log_entry_properties,
                    fixture.close_log_entry_data.object_references,
                    fixture.close_log_entry_data.result_code_or_name,
                    fixture.close_log_entry_data.session_ident),
            ::Fred::LibLogger::LogEntryDoesNotExist);
}

BOOST_FIXTURE_TEST_CASE(all_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasAllCloseLogEntryData>>)
{
    BOOST_CHECK_NO_THROW(
            ::Fred::LibLogger::close_log_entry(
                    fixture.transaction,
                    fixture.close_log_entry_data.log_entry_ident,
                    fixture.close_log_entry_data.log_entry_content,
                    fixture.close_log_entry_data.log_entry_properties,
                    fixture.close_log_entry_data.object_references,
                    fixture.close_log_entry_data.result_code_or_name,
                    fixture.close_log_entry_data.session_ident));
}

BOOST_FIXTURE_TEST_CASE(required_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasRequiredCloseLogEntryData>>)
{
    //BOOST_CHECK_NO_THROW(
            ::Fred::LibLogger::close_log_entry(
                    fixture.transaction,
                    fixture.close_log_entry_data.log_entry_ident,
                    fixture.close_log_entry_data.log_entry_content,
                    fixture.close_log_entry_data.log_entry_properties,
                    fixture.close_log_entry_data.object_references,
                    fixture.close_log_entry_data.result_code_or_name,
                    fixture.close_log_entry_data.session_ident);//);
}

BOOST_AUTO_TEST_SUITE_END() // TestCloseLogEntry

} // namespace Test

/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"

#include "liblogger/register_log_entry_types.hh"
#include "liblogger/register_object_reference_types.hh"
#include "liblogger/register_results.hh"
#include "liblogger/register_service.hh"

#include "libpg/query.hh"
#include "libpg/util/strong_type_operators.hh"

#include "liblog/liblog.hh"

#include <stdexcept>

namespace Test {
namespace Fixture {

// empty

Empty::Empty(const LibPg::PgRwTransaction&)
{
}

// create_session

HasEmptyCreateSessionData::HasEmptyCreateSessionData(const LibPg::PgRwTransaction&)
{
}

HasAllCreateSessionData::HasAllCreateSessionData(const LibPg::PgRwTransaction&)
    : create_session_data{
              ::Fred::LibLogger::LogEntry::UserName{"username"},
              ::Fred::LibLogger::LogEntry::UserId{1}}
{
}

HasRequiredCreateSessionData::HasRequiredCreateSessionData(const LibPg::PgRwTransaction&)
    : create_session_data{
              ::Fred::LibLogger::LogEntry::UserName{"username"},
              boost::none}
{
}

// close_session

HasCloseSessionDataEmptyDb::HasCloseSessionDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : close_session_data{
              ::Fred::LibLogger::SessionIdent{
                      ::Fred::LibLogger::LogEntry::SessionId{1},
                      ::Fred::LibLogger::LogEntry::SessionLoginTime{
                              boost::posix_time::from_iso_string("19700101T000000.000")}}}
{
    exec(_rw_transaction, "DELETE FROM session");
}

HasCloseSessionDataFullDb::HasCloseSessionDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : close_session_data{
              ::Fred::LibLogger::create_session(
                      _rw_transaction,
                      ::Fred::LibLogger::LogEntry::UserName{"usernme"},
                      ::Fred::LibLogger::LogEntry::UserId{1})
                      .session_ident}
{
}

// get_services

HasGetServicesDataEmptyDb::HasGetServicesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : get_services_result{}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
}

HasGetServicesDataFullDb::HasGetServicesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : get_services_result{
              {::Fred::LibLogger::LogEntry::ServiceName{"Service"}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
}

// create_log_entry

HasEmptyCreateLogEntryData::HasEmptyCreateLogEntryData(const LibPg::PgRwTransaction&)
{
}

HasAllCreateLogEntryData::HasAllCreateLogEntryData(const LibPg::PgRwTransaction& _rw_transaction)
    : create_log_entry_data{
              [&]() {
                  boost::system::error_code boost_error_code;
                  const auto ip =
                          boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{
                                  boost::asio::ip::address::from_string("127.0.0.1", boost_error_code)};
                  if (boost_error_code)
                  {
                      throw std::runtime_error("invalid ip address");
                  }
                  return ip;
              }(),
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
              boost::optional<::Fred::LibLogger::LogEntry::Content>{"create-log-entry-content"},
              std::vector<::Fred::LibLogger::LogEntryProperty>{
                      ::Fred::LibLogger::LogEntryProperty{
                              ::Fred::LibLogger::LogEntry::PropertyType{"new-property-type"},
                              std::vector<::Fred::LibLogger::LogEntryPropertyValue>{
                                      ::Fred::LibLogger::LogEntryPropertyValue{
                                              ::Fred::LibLogger::LogEntry::PropertyValue{"value1"},
                                              std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}},
                                      ::Fred::LibLogger::LogEntryPropertyValue{
                                              ::Fred::LibLogger::LogEntry::PropertyValue{"value2"},
                                              std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}}}}},
              std::vector<::Fred::LibLogger::ObjectReferences>{
                      ::Fred::LibLogger::ObjectReferences{
                              ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType"},
                              std::vector<::Fred::LibLogger::LogEntry::ObjectIdent>{
                                      ::Fred::LibLogger::LogEntry::ObjectIdent{"1"}}}},
              ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
              boost::optional<::Fred::LibLogger::SessionIdent>{}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
    exec(_rw_transaction, "DELETE FROM request_object_ref");
    exec(_rw_transaction, "DELETE FROM request_object_type");
    ::Fred::LibLogger::register_object_reference_types(
            _rw_transaction,
            {::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType"}});
}

HasRequiredCreateLogEntryData::HasRequiredCreateLogEntryData(const LibPg::PgRwTransaction& _rw_transaction)
    : create_log_entry_data{
              boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
              boost::optional<::Fred::LibLogger::LogEntry::Content>{},
              std::vector<::Fred::LibLogger::LogEntryProperty>{},
              std::vector<::Fred::LibLogger::ObjectReferences>{},
              ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
              boost::optional<::Fred::LibLogger::SessionIdent>{}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
}

// close_log_entry

HasCloseLogEntryDataOfNonexistentLogEntry::HasCloseLogEntryDataOfNonexistentLogEntry(const LibPg::PgRwTransaction& _rw_transaction)
    : close_log_entry_data{
              ::Fred::LibLogger::LogEntryIdent{
                  ::Fred::LibLogger::LogEntry::LogEntryId{1},
                  ::Fred::LibLogger::LogEntry::TimeBegin{boost::posix_time::second_clock::local_time()},
                  ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                  ::Fred::LibLogger::LogEntry::IsMonitoring{true}},
              boost::optional<::Fred::LibLogger::LogEntry::Content>{},
              std::vector<::Fred::LibLogger::LogEntryProperty>{},
              std::vector<::Fred::LibLogger::ObjectReferences>{},
              boost::variant<::Fred::LibLogger::LogEntry::ResultCode, ::Fred::LibLogger::LogEntry::ResultName>{},
              boost::optional<::Fred::LibLogger::SessionIdent>{}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
};

HasAllCloseLogEntryData::HasAllCloseLogEntryData(const LibPg::PgRwTransaction& _rw_transaction)
    : close_log_entry_data{
              [&]() {
                  exec(_rw_transaction, "DELETE FROM request_type");
                  exec(_rw_transaction, "DELETE FROM result_code");
                  exec(_rw_transaction, "DELETE FROM service");
                  ::Fred::LibLogger::register_service(
                          _rw_transaction,
                          ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                          ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
                  ::Fred::LibLogger::register_log_entry_types(
                          _rw_transaction,
                          ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                          {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
                  return ::Fred::LibLogger::create_log_entry(
                          _rw_transaction,
                          boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
                          ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                          ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                          boost::optional<::Fred::LibLogger::LogEntry::Content>{},
                          std::vector<::Fred::LibLogger::LogEntryProperty>{},
                          std::vector<::Fred::LibLogger::ObjectReferences>{},
                          ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
                          boost::optional<::Fred::LibLogger::SessionIdent>{})
                          .log_entry_ident;
              }(),
              boost::optional<::Fred::LibLogger::LogEntry::Content>{"close-log-entry-content"},
              std::vector<::Fred::LibLogger::LogEntryProperty>{
                      ::Fred::LibLogger::LogEntryProperty{
                              ::Fred::LibLogger::LogEntry::PropertyType{"new-property-type"},
                              std::vector<::Fred::LibLogger::LogEntryPropertyValue>{
                                      ::Fred::LibLogger::LogEntryPropertyValue{
                                              ::Fred::LibLogger::LogEntry::PropertyValue{"value1"},
                                              std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}},
                                      ::Fred::LibLogger::LogEntryPropertyValue{
                                              ::Fred::LibLogger::LogEntry::PropertyValue{"value2"},
                                              std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}}}}},
              std::vector<::Fred::LibLogger::ObjectReferences>{
                      ::Fred::LibLogger::ObjectReferences{
                              ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType"},
                              std::vector<::Fred::LibLogger::LogEntry::ObjectIdent>{
                                      ::Fred::LibLogger::LogEntry::ObjectIdent{"1"}}}},
              boost::variant<::Fred::LibLogger::LogEntry::ResultCode, ::Fred::LibLogger::LogEntry::ResultName>{},
              boost::optional<::Fred::LibLogger::SessionIdent>{}}
{
    ::Fred::LibLogger::register_object_reference_types(
            _rw_transaction,
            {::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType"}});
}

HasRequiredCloseLogEntryData::HasRequiredCloseLogEntryData(const LibPg::PgRwTransaction& _rw_transaction)
    : close_log_entry_data{
              [&]() {
                  exec(_rw_transaction, "DELETE FROM request_type");
                  exec(_rw_transaction, "DELETE FROM result_code");
                  exec(_rw_transaction, "DELETE FROM service");
                  ::Fred::LibLogger::register_service(
                          _rw_transaction,
                          ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                          ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
                  ::Fred::LibLogger::register_log_entry_types(
                          _rw_transaction,
                          ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                          {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
                  return ::Fred::LibLogger::create_log_entry(
                          _rw_transaction,
                          boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
                          ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                          ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                          boost::optional<::Fred::LibLogger::LogEntry::Content>{},
                          std::vector<::Fred::LibLogger::LogEntryProperty>{},
                          std::vector<::Fred::LibLogger::ObjectReferences>{},
                          ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
                          boost::optional<::Fred::LibLogger::SessionIdent>{})
                          .log_entry_ident;
              }(),
              boost::optional<::Fred::LibLogger::LogEntry::Content>{},
              std::vector<::Fred::LibLogger::LogEntryProperty>{},
              std::vector<::Fred::LibLogger::ObjectReferences>{},
              boost::variant<::Fred::LibLogger::LogEntry::ResultCode, ::Fred::LibLogger::LogEntry::ResultName>{},
              boost::optional<::Fred::LibLogger::SessionIdent>{}}
{
}

// get_results

HasEmptyGetResultsData::HasEmptyGetResultsData(const LibPg::PgRwTransaction& _rw_transaction)
    : get_results_data{}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
}

HasGetResultsDataEmptyDb::HasGetResultsDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : get_results_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
}

HasGetResultsDataFullDb::HasGetResultsDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : get_results_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"}},
      get_results_result{
              {{::Fred::LibLogger::LogEntry::ResultName{"Rn"}, ::Fred::LibLogger::LogEntry::ResultCode{0}}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
    ::Fred::LibLogger::register_results(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::Result{::Fred::LibLogger::LogEntry::ResultCode{0}, ::Fred::LibLogger::LogEntry::ResultName{"Rn"}}});
}

// get_log_entry_types

HasEmptyGetLogEntryTypesData::HasEmptyGetLogEntryTypesData(const LibPg::PgRwTransaction&)
    : get_log_entry_types_data{}
{
}

HasGetLogEntryTypesDataEmptyDb::HasGetLogEntryTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : get_log_entry_types_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"}},
      get_log_entry_types_result{}
{
    exec(_rw_transaction, "DELETE FROM request_type");
}

HasGetLogEntryTypesDataFullDb::HasGetLogEntryTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : get_log_entry_types_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"}},
      get_log_entry_types_result{
              {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
}

// get_object_reference_types

HasGetObjectReferenceTypesDataEmptyDb::HasGetObjectReferenceTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
{
    exec(_rw_transaction, "DELETE FROM request_object_ref");
    exec(_rw_transaction, "DELETE FROM request_object_type");
}

HasGetObjectReferenceTypesDataFullDb::HasGetObjectReferenceTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : get_object_reference_types_result{
              {::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType"}}}
{
    exec(_rw_transaction, "DELETE FROM request_object_ref");
    exec(_rw_transaction, "DELETE FROM request_object_type");
    ::Fred::LibLogger::register_object_reference_types(
            _rw_transaction,
            {::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType"}});
}

// register_service

HasEmptyRegisterServiceData::HasEmptyRegisterServiceData(const LibPg::PgRwTransaction& _rw_transaction)
    : register_service_data{}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
}

HasRegisterServiceDataEmptyDb::HasRegisterServiceDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : register_service_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
              ::Fred::LibLogger::LogEntry::ServiceHandle{"service"}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
}

// register_results

HasEmptyRegisterResultsData::HasEmptyRegisterResultsData(const LibPg::PgRwTransaction& _rw_transaction)
    : register_results_data{}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
}

HasRegisterResultsDataEmptyDb::HasRegisterResultsDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : register_results_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
              {
                  ::Fred::LibLogger::Result{
                          ::Fred::LibLogger::LogEntry::ResultCode{1000},
                          ::Fred::LibLogger::LogEntry::ResultName{"ResultSuccess"}},
                  ::Fred::LibLogger::Result{
                          ::Fred::LibLogger::LogEntry::ResultCode{5000},
                          ::Fred::LibLogger::LogEntry::ResultName{"ResultFailure"}}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
}

HasRegisterResultsDataFullDb::HasRegisterResultsDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : register_results_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
              {
                  ::Fred::LibLogger::Result{
                          ::Fred::LibLogger::LogEntry::ResultCode{1000},
                          ::Fred::LibLogger::LogEntry::ResultName{"ResultSuccess"}},
                  ::Fred::LibLogger::Result{
                          ::Fred::LibLogger::LogEntry::ResultCode{5000},
                          ::Fred::LibLogger::LogEntry::ResultName{"ResultFailure"}}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            register_results_data.service,
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
}

// register_log_entry_types

HasEmptyRegisterLogEntryTypesData::HasEmptyRegisterLogEntryTypesData(const LibPg::PgRwTransaction&)
    : register_log_entry_types_data{}
{
}

HasRegisterLogEntryTypesDataEmptyDb::HasRegisterLogEntryTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : register_log_entry_types_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
              {
                  ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType1"},
                  ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType2"}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
}

HasRegisterLogEntryTypesDataFullDb::HasRegisterLogEntryTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : register_log_entry_types_data{
              ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
              {
                  ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType1"},
                  ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType2"}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            register_log_entry_types_data.service,
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
}

// register_object_reference_types

HasEmptyRegisterObjectReferenceTypesDataEmptyDb::HasEmptyRegisterObjectReferenceTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction)
    : register_object_reference_types_data{
        {}
    }
{
    exec(_rw_transaction, "DELETE FROM request_object_ref");
    exec(_rw_transaction, "DELETE FROM request_object_type");
}

HasRegisterObjectReferenceTypesDataFullDb::HasRegisterObjectReferenceTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : register_object_reference_types_data{
              {::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType1"},
                      ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType2"}}}
{
    exec(_rw_transaction, "DELETE FROM request_object_ref");
    exec(_rw_transaction, "DELETE FROM request_object_type");
}

CleanDb::CleanDb(const LibPg::PgRwTransaction& _rw_transaction)
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");
    exec(_rw_transaction, "DELETE FROM request_object_ref");
    exec(_rw_transaction, "DELETE FROM request_object_type");
}

namespace {

std::tuple<::Fred::LibLogger::LogEntryIdent, ::Fred::LibLogger::LogEntryInfo> minimal_log_entry(const LibPg::PgRwTransaction& _rw_transaction)
{
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
    ::Fred::LibLogger::register_results(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::Result{
                    ::Fred::LibLogger::LogEntry::ResultCode{1},
                    ::Fred::LibLogger::LogEntry::ResultName{"Result1"}}});
    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
    const auto create_log_entry_reply =
            ::Fred::LibLogger::create_log_entry(
                    _rw_transaction,
                    boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
                    ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                    ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                    boost::optional<::Fred::LibLogger::LogEntry::Content>{}, // raw request
                    std::vector<::Fred::LibLogger::LogEntryProperty>{}, // input properties
                    std::vector<::Fred::LibLogger::ObjectReferences>{},
                    ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
                    boost::optional<::Fred::LibLogger::SessionIdent>{});
    ::Fred::LibLogger::close_log_entry(
            _rw_transaction,
            create_log_entry_reply.log_entry_ident,
            boost::optional<::Fred::LibLogger::LogEntry::Content>{}, // raw response
            std::vector<::Fred::LibLogger::LogEntryProperty>{}, // output properties
            std::vector<::Fred::LibLogger::ObjectReferences>{},
            boost::variant<::Fred::LibLogger::LogEntry::ResultCode, ::Fred::LibLogger::LogEntry::ResultName>{::Fred::LibLogger::LogEntry::ResultCode{1}},
            boost::optional<::Fred::LibLogger::SessionIdent>{});
    return std::tuple<::Fred::LibLogger::LogEntryIdent, ::Fred::LibLogger::LogEntryInfo>{
            create_log_entry_reply.log_entry_ident,
            ::Fred::LibLogger::LogEntryInfo{
                    create_log_entry_reply.log_entry_ident.time_begin,
                    boost::optional<::Fred::LibLogger::LogEntry::TimeEnd>{},
                    ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                    boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
                    boost::optional<::Fred::LibLogger::LogEntry::LogEntryTypeName>{"LogEntryType"},
                    boost::optional<::Fred::LibLogger::LogEntry::ResultCode>{1},
                    boost::optional<::Fred::LibLogger::LogEntry::ResultName>{"Result1"},
                    boost::optional<::Fred::LibLogger::LogEntry::RawRequest>{}, // content / raw_request
                    boost::optional<::Fred::LibLogger::LogEntry::RawResponse>{}, // content / raw_response
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{""}, // missing user name is represented by empty string in the detabase
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{},
                    std::vector<::Fred::LibLogger::LogEntryProperty>{}, // input_properties
                    std::vector<::Fred::LibLogger::LogEntryProperty>{}, // output_properties
                    std::vector<::Fred::LibLogger::ObjectReferences>{}}};
}

std::tuple<::Fred::LibLogger::LogEntryIdent, ::Fred::LibLogger::LogEntryInfo> full_log_entry(const LibPg::PgRwTransaction& _rw_transaction)
{
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
    ::Fred::LibLogger::register_results(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::Result{
                    ::Fred::LibLogger::LogEntry::ResultCode{1},
                    ::Fred::LibLogger::LogEntry::ResultName{"Result1"}}});
    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
    ::Fred::LibLogger::register_object_reference_types(
            _rw_transaction,
            {::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType1"},
            ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType2"}});
    const auto create_session_reply =
            ::Fred::LibLogger::create_session(
                    _rw_transaction,
                    ::Fred::LibLogger::LogEntry::UserName{"username"},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{42});
    ::Fred::LibLogger::close_session(
            _rw_transaction,
            create_session_reply.session_ident);
    const auto input_properties =
            std::vector<::Fred::LibLogger::LogEntryProperty>{
                    ::Fred::LibLogger::LogEntryProperty{
                            ::Fred::LibLogger::LogEntry::PropertyType{"PropertyType1"},
                            {::Fred::LibLogger::LogEntryPropertyValue{
                                    ::Fred::LibLogger::LogEntry::PropertyValue{"PropertyType1Value1"},
                                    std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}}}},
                    ::Fred::LibLogger::LogEntryProperty{
                            ::Fred::LibLogger::LogEntry::PropertyType{"PropertyType2"},
                            {::Fred::LibLogger::LogEntryPropertyValue{
                                    ::Fred::LibLogger::LogEntry::PropertyValue{"PropertyType2Value2"},
                                    std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}}}}};
    const auto output_properties =
            std::vector<::Fred::LibLogger::LogEntryProperty>{
                    ::Fred::LibLogger::LogEntryProperty{
                            ::Fred::LibLogger::LogEntry::PropertyType{"PropertyType1"},
                            {::Fred::LibLogger::LogEntryPropertyValue{
                                    ::Fred::LibLogger::LogEntry::PropertyValue{"PropertyType1Value1"},
                                    std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}}}},
                    ::Fred::LibLogger::LogEntryProperty{
                            ::Fred::LibLogger::LogEntry::PropertyType{"PropertyType2"},
                            {::Fred::LibLogger::LogEntryPropertyValue{
                                    ::Fred::LibLogger::LogEntry::PropertyValue{"PropertyType2Value2"},
                                    std::map<::Fred::LibLogger::LogEntry::PropertyType, std::vector<::Fred::LibLogger::LogEntry::PropertyValue>>{}}}}};
    const auto object_references =
            std::vector<::Fred::LibLogger::ObjectReferences>{
                    ::Fred::LibLogger::ObjectReferences{
                            ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType1"},
                            {::Fred::LibLogger::LogEntry::ObjectIdent{"ObjectType1Ident1"},
                                    ::Fred::LibLogger::LogEntry::ObjectIdent{"ObjectType1Ident2"}}},
                    ::Fred::LibLogger::ObjectReferences{
                            ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName{"ObjectType2"},
                            {::Fred::LibLogger::LogEntry::ObjectIdent{"ObjectType2Ident1"},
                                    ::Fred::LibLogger::LogEntry::ObjectIdent{"ObjectType2Ident2"}}}};
    const auto create_log_entry_reply =
            ::Fred::LibLogger::create_log_entry(
                    _rw_transaction,
                    boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{boost::asio::ip::address::from_string("127.0.0.1")},
                    ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                    ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                    boost::optional<::Fred::LibLogger::LogEntry::Content>{"Raw Request"}, // raw request
                    input_properties,
                    object_references,
                    ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
                    create_session_reply.session_ident);
    ::Fred::LibLogger::close_log_entry(
            _rw_transaction,
            create_log_entry_reply.log_entry_ident,
            boost::optional<::Fred::LibLogger::LogEntry::Content>{"Raw Response"}, // raw response
            output_properties,
            std::vector<::Fred::LibLogger::ObjectReferences>{},
            boost::variant<::Fred::LibLogger::LogEntry::ResultCode, ::Fred::LibLogger::LogEntry::ResultName>{::Fred::LibLogger::LogEntry::ResultName{"Result1"}},
            create_session_reply.session_ident);
    return std::tuple<::Fred::LibLogger::LogEntryIdent, ::Fred::LibLogger::LogEntryInfo>{
            create_log_entry_reply.log_entry_ident,
            ::Fred::LibLogger::LogEntryInfo{
                    create_log_entry_reply.log_entry_ident.time_begin,
                    boost::optional<::Fred::LibLogger::LogEntry::TimeEnd>{},
                    ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
                    boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
                    boost::optional<::Fred::LibLogger::LogEntry::LogEntryTypeName>{"LogEntryType"},
                    boost::optional<::Fred::LibLogger::LogEntry::ResultCode>{1},
                    boost::optional<::Fred::LibLogger::LogEntry::ResultName>{"Result1"},
                    boost::optional<::Fred::LibLogger::LogEntry::RawRequest>{"Raw Request"},
                    boost::optional<::Fred::LibLogger::LogEntry::RawResponse>{"Raw Response"},
                    boost::optional<::Fred::LibLogger::LogEntry::UserName>{"username"},
                    boost::optional<::Fred::LibLogger::LogEntry::UserId>{42},
                    input_properties,
                    output_properties,
                    object_references}};
}

} // namespace Test::Fixture::{anonymous}

HasGetLogEntryInfoDataFullDb::HasGetLogEntryInfoDataFullDb(const LibPg::PgRwTransaction& _rw_transaction)
    : clean_db{CleanDb{_rw_transaction}},
      minimal_log_entry_data{
              minimal_log_entry(_rw_transaction)},
      full_log_entry_data{
              full_log_entry(_rw_transaction)},
      nonexistent_log_entry_data{
              minimal_log_entry(_rw_transaction)}
{
    exec(_rw_transaction,
            LibPg::make_query() <<
            "DELETE FROM request WHERE id = " << LibPg::parameter<::Fred::LibLogger::LogEntry::LogEntryId>().as_big_int(),
            std::get<::Fred::LibLogger::LogEntryIdent>(nonexistent_log_entry_data).id);
}

//

HasNoLogEntries::HasNoLogEntries(const LibPg::PgRwTransaction& _rw_transaction)
    : time_end{::Fred::LibLogger::LogEntry::TimeEnd{boost::posix_time::second_clock::local_time()}},
      time_begin{::Fred::LibLogger::LogEntry::TimeBegin{time_end.get() - boost::gregorian::days(10)}},
      service{"Service"},
      log_entry_types{{::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");

    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});

    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
}

HasSeveralLogEntries::HasSeveralLogEntries(const LibPg::PgRwTransaction& _rw_transaction)
    : time_end{::Fred::LibLogger::LogEntry::TimeEnd{boost::posix_time::second_clock::local_time()}},
      time_begin{::Fred::LibLogger::LogEntry::TimeBegin{time_end.get() - boost::gregorian::days(10)}},
      service{"Service"},
      log_entry_types{{::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}}}
{
    exec(_rw_transaction, "DELETE FROM request_type");
    exec(_rw_transaction, "DELETE FROM result_code");
    exec(_rw_transaction, "DELETE FROM service");

    //const ::Fred::LibLogger::LogEntry::LogEntryObjectTypeName object_type{"domain"};
    //const ::Fred::LibLogger::LogEntry::ObjectId object_id{0};
    //const ::Fred::LibLogger::LogEntry::ObjectIdent object_ident{"222"};

    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service"});
    ::Fred::LibLogger::register_service(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service2"},
            ::Fred::LibLogger::LogEntry::ServiceHandle{"service2"});

    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});
    ::Fred::LibLogger::register_log_entry_types(
            _rw_transaction,
            ::Fred::LibLogger::LogEntry::ServiceName{"Service2"},
            {::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"}});

    ::Fred::LibLogger::LogEntry::ServiceName log_entry_service{"Service"};
    for (std::size_t i = 0; i < 12; ++i)
    {
        if (i % 2 == 0)
        {
            log_entry_service = ::Fred::LibLogger::LogEntry::ServiceName{"Service"};
        }
        else
        {
            log_entry_service = ::Fred::LibLogger::LogEntry::ServiceName{"Service2"};
        }

        if (i < 5)
        {
            ::Fred::LibLogger::create_log_entry(
                    _rw_transaction,
                    boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
                    ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                    log_entry_service,
                    boost::optional<::Fred::LibLogger::LogEntry::Content>{},
                    std::vector<::Fred::LibLogger::LogEntryProperty>{},
                    std::vector<::Fred::LibLogger::ObjectReferences>{},
                    ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
                    boost::optional<::Fred::LibLogger::SessionIdent>{});
        }
        else
        {
            ::Fred::LibLogger::create_log_entry(
                    _rw_transaction,
                    boost::optional<::Fred::LibLogger::LogEntry::SourceIp>{},
                    ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                    log_entry_service,
                    boost::optional<::Fred::LibLogger::LogEntry::Content>{},
                    std::vector<::Fred::LibLogger::LogEntryProperty>{},
                    std::vector<::Fred::LibLogger::ObjectReferences>{},
                    ::Fred::LibLogger::LogEntry::LogEntryTypeName{"LogEntryType"},
                    ::Fred::LibLogger::create_session(
                            _rw_transaction,
                            ::Fred::LibLogger::LogEntry::UserName{"usernme"},
                            ::Fred::LibLogger::LogEntry::UserId{1})
                            .session_ident);
        }
    }
}

} // namespace Test::Fixture
} // namespace Test

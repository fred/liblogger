/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CFG_HH_4390857D98304A6BBB9DBA3244E95482
#define CFG_HH_4390857D98304A6BBB9DBA3244E95482

#include "liblog/level.hh"

#include <boost/blank.hpp>
#include <boost/optional.hpp>
#include <boost/program_options.hpp>
#include <boost/variant.hpp>

#include <stdexcept>
#include <string>

namespace Test {
namespace Setup {
namespace Fixture {
namespace Cfg {

class HelpRequested : public std::exception
{
public:
    explicit HelpRequested(const std::string& msg);
    const char* what()const noexcept override;
private:
    const std::string msg_;
};

struct Exception : std::runtime_error
{
    explicit Exception(const std::string& msg);
};

struct UnknownOption : Exception
{
    explicit UnknownOption(const std::string& msg);
};

struct MissingOption : Exception
{
    explicit MissingOption(const std::string& msg);
};

class Options
{
public:
    Options() = delete;
    Options(const Options&) = delete;
    Options(Options&&) = delete;
    Options& operator=(const Options&) = delete;
    Options& operator=(Options&&) = delete;

    static const Options& get();
    static const Options& init(int argc, const char* const* argv);

    boost::optional<std::string> config_file_name;
    struct Database
    {
        boost::optional<std::string> host;
        boost::optional<std::string> host_addr;
        boost::optional<int> port;
        boost::optional<std::string> user;
        boost::optional<std::string> dbname;
        boost::optional<std::string> password;
        boost::optional<int> timeout;
    };
    Database database;
    Database admin_database;
    struct Log
    {
        struct Console
        {
            boost::optional<LibLog::Level> min_severity;
        };
        struct Logfile
        {
            std::string file_name;
            boost::optional<LibLog::Level> min_severity;
        };
        struct Syslog
        {
            int facility;
            boost::optional<LibLog::Level> min_severity;
        };
        boost::variant<boost::blank, Console, Logfile, Syslog> device;
    } log;
    struct Logger
    {
        std::string monitoring_hosts_file;
    } logger;
private:
    Options(int argc, const char* const* argv);
};

} // namespace Test::Setup::Fixture::Cfg
} // namespace Test::Setup::Fixture
} // namespace Test::Setup
} // namespace Test

#endif

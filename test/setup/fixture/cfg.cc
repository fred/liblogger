/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "setup/fixture/cfg.hh"

#include "liblog/level.hh"

#include <iostream>
#include <fstream>
#include <sstream>

namespace Test {
namespace Setup {
namespace Fixture {
namespace Cfg {

namespace {

LibLog::Level make_severity(const std::string& str)
{
    if (str == "crit")
    {
        return LibLog::Level::critical;
    }
    if (str == "err")
    {
        return LibLog::Level::error;
    }
    if (str == "warning")
    {
        return LibLog::Level::warning;
    }
    if (str == "info")
    {
        return LibLog::Level::info;
    }
    if (str == "debug")
    {
        return LibLog::Level::debug;
    }
    if (str == "trace")
    {
        return LibLog::Level::trace;
    }
    throw Exception{"not a valid severity \"" + str + "\""};
}

void required_one_of(const boost::program_options::variables_map& variables_map, const char* variable)
{
    if (variables_map.count(variable) == 0)
    {
        throw MissingOption{"missing one of options: '" + std::string{variable} + "'"};
    }
}

template <typename ...Ts>
void required_one_of(
        const boost::program_options::variables_map& variables_map,
        const char* first_variable,
        const Ts* ...other_variables)
{
    if (0 < variables_map.count(first_variable))
    {
        return;
    }
    try
    {
        required_one_of(variables_map, other_variables...);
    }
    catch (const MissingOption& e)
    {
        throw MissingOption{e.what() + std::string{", '"} + first_variable + "'"};
    }
}

constexpr int invalid_argc = -1;
constexpr const char* const invalid_argv[0] = { };

} // namespace Test::Setup::Fixture::Cfg::{anonymous}

const Options& Options::get()
{
    return init(invalid_argc, invalid_argv);
}

const Options& Options::init(int argc, const char* const* argv)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (argc != invalid_argc) || (argv != invalid_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            throw std::runtime_error{"First call of Test::Setup::Fixture::Cfg::Options::init must contain valid arguments"};
        }
        static const Options singleton(argc, argv);
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        throw std::runtime_error{"Only first call of Test::Setup::Fixture::Cfg::Options::init can contain valid arguments"};
    }
    return *singleton_ptr;
}

Options::Options(int argc, const char* const* argv)
{
    boost::program_options::options_description command_line_only_options("Generic options (only available on command line)");
    std::string opt_config_file_name;
    command_line_only_options.add_options()
        ("help,h", "produce help message")
        ("config,c",
         boost::program_options::value<std::string>(&opt_config_file_name)->default_value("test-liblogger.conf"),
         "name of a file of a configuration.");

    boost::program_options::options_description database_options("Database options");
    std::string database_host;
    std::string database_host_addr;
    int database_port;
    std::string database_user;
    std::string database_dbname;
    std::string database_password;
    int database_timeout;
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>(&database_host), "name of host to connect to")
        ("database.host_addr", boost::program_options::value<std::string>(&database_host_addr), "ip address of host to connect to")
        ("database.port", boost::program_options::value<int>(&database_port), "port number to connect to at the server host")
        ("database.user", boost::program_options::value<std::string>(&database_user), "PostgreSQL database user name to connect as")
        ("database.dbname", boost::program_options::value<std::string>(&database_dbname), "PostgreSQL database name to connect to")
        ("database.password", boost::program_options::value<std::string>(&database_password), "password used for password authentication")
        ("database.timeout", boost::program_options::value<int>(&database_timeout), "database connection timeout");

    boost::program_options::options_description log_options("Logging options");
    std::string log_device;
    std::string log_default_min_severity;
    log_options.add_options()
        ("log.device", boost::program_options::value<std::string>(&log_device), "where to log (console/file/syslog)")
        ("log.min_severity", boost::program_options::value<std::string>(&log_default_min_severity),
         "do not log more trivial events; "
         "severity in descending order: crit/err/warning/info/debug/trace");

    boost::program_options::options_description log_console_options("Logging on console options");
    std::string log_console_min_severity;
    log_console_options.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>(&log_console_min_severity),
         "do not log more trivial events; "
         "severity in descending order: crit/err/warning/info/debug/trace");

    boost::program_options::options_description log_file_options("Logging into file options");
    std::string log_file_name;
    std::string log_file_min_severity;
    log_file_options.add_options()
        ("log.file.file_name",
         boost::program_options::value<std::string>(&log_file_name),
         "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>(&log_file_min_severity),
         "do not log more trivial events; "
         "severity in descending order: crit/err/warning/info/debug/trace");

    boost::program_options::options_description log_syslog_options("Logging into syslog options");
    int log_syslog_facility;
    std::string log_syslog_min_severity;
    log_syslog_options.add_options()
        ("log.syslog.facility",
         boost::program_options::value<int>(&log_syslog_facility)->default_value(2),
         "what facility to log with (in range 0..7)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>(&log_syslog_min_severity),
         "do not log more trivial events; "
         "severity in descending order: crit/err/warning/info/debug/trace");

    boost::program_options::options_description logger_options("Server options");
    std::string logger_monitoring_hosts_file;
    logger_options.add_options()
        ("logger.monitoring_hosts_file", boost::program_options::value<std::string>(&logger_monitoring_hosts_file), "monitoring hosts file.");

    boost::program_options::options_description database_admin_options("Database admin options");
    std::string admin_host;
    int admin_port;
    std::string admin_user;
    std::string admin_dbname;
    std::string admin_password;
    int admin_timeout;
    database_admin_options.add_options()
        ("database.admin.host", boost::program_options::value<std::string>(&admin_host), "name of host to connect to")
        ("database.admin.port", boost::program_options::value<int>(&admin_port), "port number to connect to at the server host")
        ("database.admin.user", boost::program_options::value<std::string>(&admin_user), "PostgreSQL user name to connect as")
        ("database.admin.dbname", boost::program_options::value<std::string>(&admin_dbname), "PostgreSQL database name to connect to")
        ("database.admin.timeout", boost::program_options::value<int>(&admin_timeout), "database connection timeout");

    boost::program_options::options_description command_line_options("test-logger options");
    command_line_options
        .add(command_line_only_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_admin_options)
        .add(database_options)
        .add(logger_options);
    boost::program_options::options_description config_file_options;
    config_file_options
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_admin_options)
        .add(database_options)
        .add(logger_options);

    boost::program_options::variables_map variables_map;
    try
    {
        boost::program_options::store(
                boost::program_options::command_line_parser(argc, argv)
                    .options(command_line_options)
                    .run(),
                variables_map);
    }
    catch (const boost::program_options::unknown_option& unknown_option)
    {
        std::ostringstream out;
        out << unknown_option.what() << "\n\n" << command_line_options;
        throw UnknownOption{out.str()};
    }
    boost::program_options::notify(variables_map);

    if (0 < variables_map.count("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        throw HelpRequested{out.str()};
    }
    const bool config_file_name_presents = 0 < variables_map.count("config");
    if (config_file_name_presents)
    {
        this->config_file_name = opt_config_file_name;
        std::ifstream config_file(opt_config_file_name);
        if (!config_file)
        {
            throw Exception{"can not open config file \"" + opt_config_file_name + "\""};
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options),
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            throw UnknownOption{out.str()};
        }
        boost::program_options::notify(variables_map);
    }

    if (0 < variables_map.count("database.host"))
    {
        this->database.host = database_host;
    }
    if (0 < variables_map.count("database.port"))
    {
        this->database.port = database_port;
    }
    if (0 < variables_map.count("database.user"))
    {
        this->database.user = database_user;
    }
    if (0 < variables_map.count("database.dbname"))
    {
        this->database.dbname = database_dbname;
    }
    if (0 < variables_map.count("database.password"))
    {
        this->database.password = database_password;
    }
    if (0 < variables_map.count("database.timeout"))
    {
        this->database.timeout = database_timeout;
    }

    if (0 < variables_map.count("database.admin.host"))
    {
        this->admin_database.host = admin_host;
    }
    if (0 < variables_map.count("database.admin.port"))
    {
        this->admin_database.port = admin_port;
    }
    if (0 < variables_map.count("database.admin.user"))
    {
        this->admin_database.user = admin_user;
    }
    if (0 < variables_map.count("database.admin.dbname"))
    {
        this->admin_database.dbname = admin_dbname;
    }
    if (0 < variables_map.count("database.admin.password"))
    {
        this->admin_database.password = admin_password;
    }
    if (0 < variables_map.count("database.admin.timeout"))
    {
        this->admin_database.timeout = admin_timeout;
    }

    if (0 < variables_map.count("log.device"))
    {
        boost::optional<LibLog::Level> log_min_severity;
        if (0 < variables_map.count("log.min_severity"))
        {
            log_min_severity = make_severity(log_default_min_severity);
        }
        if (log_device == "console")
        {
            required_one_of(variables_map, "log.min_severity", "log.console.min_severity");
            Log::Console console;
            if (0 < variables_map.count("log.console.min_severity"))
            {
                console.min_severity = make_severity(log_console_min_severity);
            }
            this->log.device = console;
        }
        else if (log_device == "file")
        {
            required_one_of(variables_map, "log.file.file_name");
            required_one_of(variables_map, "log.min_severity", "log.file.min_severity");
            Log::Logfile log_file;
            log_file.file_name = log_file_name;
            if (0 < variables_map.count("log.file.min_severity"))
            {
                log_file.min_severity = make_severity(log_file_min_severity);
            }
            this->log.device = log_file;
        }
        else if (log_device == "syslog")
        {
            required_one_of(variables_map, "log.syslog.facility");
            required_one_of(variables_map, "log.min_severity", "log.syslog.min_severity");
            if ((log_syslog_facility < 0) || (7 < log_syslog_facility))
            {
                throw Exception{"option 'log.syslog.facility' out of range [0, 7]"};
            }
            Log::Syslog log_syslog;
            log_syslog.facility = log_syslog_facility;
            if (0 < variables_map.count("log.syslog.min_severity"))
            {
                log_syslog.min_severity = make_severity(log_syslog_min_severity);
            }
            this->log.device = log_syslog;
        }
        else
        {
            throw Exception{"Invalid value of log.device"};
        }
    }
    required_one_of(variables_map, "logger.monitoring_hosts_file");
    this->logger.monitoring_hosts_file = logger_monitoring_hosts_file;
}

HelpRequested::HelpRequested(const std::string& msg)
    : msg_(msg)
{
}

const char* HelpRequested::what()const noexcept
{
    return msg_.c_str();
}

Exception::Exception(const std::string& msg)
    : std::runtime_error(msg)
{
}

UnknownOption::UnknownOption(const std::string& msg)
    : Exception(msg)
{
}

MissingOption::MissingOption(const std::string& msg)
    : Exception(msg)
{
}

} // namespace Test::Setup::Fixture::Cfg
} // namespace Test::Setup::Fixture
} // namespace Test::Setup
} // namespace Test

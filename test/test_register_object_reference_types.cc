/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/register_object_reference_types.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestRegisterObjectReferenceTypes)

BOOST_FIXTURE_TEST_CASE(empty_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyRegisterObjectReferenceTypesDataEmptyDb>>)
{
    for (auto iteration = 0; iteration < 2; ++iteration)
    {
        BOOST_CHECK_NO_THROW(
                ::Fred::LibLogger::register_object_reference_types(
                        fixture.transaction,
                        fixture.register_object_reference_types_data.object_reference_types));

        const auto get_object_reference_types_result =
                ::Fred::LibLogger::get_object_reference_types(
                        fixture.transaction);

        BOOST_CHECK(!get_object_reference_types_result.object_reference_types.empty());
        BOOST_CHECK_EQUAL(get_object_reference_types_result.object_reference_types.size(), fixture.register_object_reference_types_data.object_reference_types.size());
        for (auto& object_reference_type : fixture.register_object_reference_types_data.object_reference_types)
        {
            BOOST_CHECK(std::find(get_object_reference_types_result.object_reference_types.cbegin(), get_object_reference_types_result.object_reference_types.cend(), object_reference_type) != get_object_reference_types_result.object_reference_types.cend());
        }
    }
}

BOOST_FIXTURE_TEST_CASE(full_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterObjectReferenceTypesDataFullDb>>)
{
    for (auto iteration = 0; iteration < 2; ++iteration)
    {
        BOOST_CHECK_NO_THROW(
                ::Fred::LibLogger::register_object_reference_types(
                        fixture.transaction,
                        fixture.register_object_reference_types_data.object_reference_types));

        const auto get_object_reference_types_result =
                ::Fred::LibLogger::get_object_reference_types(
                        fixture.transaction);

        BOOST_CHECK(!get_object_reference_types_result.object_reference_types.empty());
        BOOST_CHECK_EQUAL(get_object_reference_types_result.object_reference_types.size(), fixture.register_object_reference_types_data.object_reference_types.size());
        for (auto& object_reference_type : fixture.register_object_reference_types_data.object_reference_types)
        {
            BOOST_CHECK(std::find(get_object_reference_types_result.object_reference_types.cbegin(), get_object_reference_types_result.object_reference_types.cend(), object_reference_type) != get_object_reference_types_result.object_reference_types.cend());
        }
    }
}

BOOST_AUTO_TEST_SUITE_END() // TestRegisterObjectReferenceTypes

} // namespace Test

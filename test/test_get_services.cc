/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/get_services.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestGetServices)

BOOST_FIXTURE_TEST_CASE(empty_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetServicesDataEmptyDb>>)
{
    const auto result =
            ::Fred::LibLogger::get_services(
                    fixture.transaction);

    BOOST_CHECK(result.log_entry_services.empty());
}

BOOST_FIXTURE_TEST_CASE(full_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetServicesDataFullDb>>)
{
    const auto result =
            ::Fred::LibLogger::get_services(
                    fixture.transaction);

    //BOOST_CHECK_EQUAL(result.log_entry_services, fixture.get_services_result.log_entry_services);
    BOOST_CHECK_EQUAL(result.log_entry_services.size(), fixture.get_services_result.log_entry_services.size());
    for (std::size_t idx = 0; idx != result.log_entry_services.size(); ++idx)
    {
        BOOST_CHECK_EQUAL(result.log_entry_services.at(idx), fixture.get_services_result.log_entry_services.at(idx));
    }
}

BOOST_AUTO_TEST_SUITE_END() // TestGetServices

} // namespace Test

/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_HH_1E78010A43AB46BA9B5C3FB65C94AA0B
#define UTIL_HH_1E78010A43AB46BA9B5C3FB65C94AA0B

#include "setup/fixture/cfg.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Test {
namespace Util {

LibPg::Dsn get_dsn(const Test::Setup::Fixture::Cfg::Options::Database& _option);

struct AutoRollbackingContext
{
    LibPg::PgRwTransaction transaction{get_dsn(Test::Setup::Fixture::Cfg::Options::get().database)};
};

template <class T>
struct SupplyCtx : AutoRollbackingContext, T
{
    SupplyCtx()
        : AutoRollbackingContext(),
          T(transaction)
    {
    }
};

template <class T>
struct AddFixture
{
    T fixture;
};

} // namespace Test::Util
} // namespace Test

#endif

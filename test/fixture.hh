/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FIXTURE_HH_1F08E69090D64E25A1F3894EC6F4B8CC
#define FIXTURE_HH_1F08E69090D64E25A1F3894EC6F4B8CC

#include "fixture/create_log_entry.hh"
#include "fixture/close_log_entry.hh"
#include "fixture/create_session.hh"
#include "fixture/close_session.hh"
#include "fixture/register_log_entry_types.hh"
#include "fixture/register_service.hh"
#include "fixture/register_results.hh"
#include "fixture/register_object_reference_types.hh"
#include "fixture/get_log_entry_types.hh"
#include "fixture/get_services.hh"
#include "fixture/get_results.hh"
#include "fixture/get_object_reference_types.hh"
#include "fixture/get_log_entry_info.hh"
#include "fixture/list_log_entries.hh"

#include "liblogger/close_log_entry.hh"
#include "liblogger/close_session.hh"
#include "liblogger/create_log_entry.hh"
#include "liblogger/create_session.hh"
#include "liblogger/get_log_entry_info.hh"
#include "liblogger/get_log_entry_types.hh"
#include "liblogger/get_object_reference_types.hh"
#include "liblogger/get_results.hh"
#include "liblogger/get_services.hh"
#include "liblogger/list_log_entries.hh"
#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"

#include "libpg/pg_rw_transaction.hh"

#include <boost/date_time/posix_time/posix_time.hpp>

#include <string>

namespace Test {
namespace Fixture {

using CreateLogEntryResult = ::Fred::LibLogger::CreateLogEntryReply;
using CreateSessionResult = ::Fred::LibLogger::CreateSessionReply;
using GetLogEntryInfoResult = ::Fred::LibLogger::GetLogEntryInfoReply;
using GetLogEntryTypesResult = ::Fred::LibLogger::GetLogEntryTypesReply;
using GetObjectReferenceTypesResult = ::Fred::LibLogger::GetObjectReferenceTypesReply;
using GetResultsResult = ::Fred::LibLogger::GetResultsReply;
using ListLogEntriesResult = ::Fred::LibLogger::ListLogEntriesReply;
using GetServicesResult = ::Fred::LibLogger::GetServicesReply;

// empty

struct Empty
{
    Empty(const LibPg::PgRwTransaction& _rw_transaction);
};

// create_session

struct HasEmptyCreateSessionData
{
    Fixture::CreateSessionData create_session_data;

    HasEmptyCreateSessionData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasAllCreateSessionData
{
    Fixture::CreateSessionData create_session_data;

    HasAllCreateSessionData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRequiredCreateSessionData
{
    Fixture::CreateSessionData create_session_data;

    HasRequiredCreateSessionData(const LibPg::PgRwTransaction& _rw_transaction);
};

// close_session

struct HasCloseSessionDataEmptyDb
{
    Fixture::CloseSessionData close_session_data;

    HasCloseSessionDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasCloseSessionDataFullDb
{
    Fixture::CloseSessionData close_session_data;

    HasCloseSessionDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// get_services

struct HasGetServicesDataEmptyDb
{
    Fixture::GetServicesResult get_services_result;

    HasGetServicesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasGetServicesDataFullDb
{
    Fixture::GetServicesResult get_services_result;

    HasGetServicesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// get_results

struct HasEmptyGetResultsData
{
    Fixture::GetResultsData get_results_data;

    HasEmptyGetResultsData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasGetResultsDataEmptyDb
{
    Fixture::GetResultsData get_results_data;

    HasGetResultsDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasGetResultsDataFullDb
{
    Fixture::GetResultsData get_results_data;
    Fixture::GetResultsResult get_results_result;

    HasGetResultsDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// get_log_entry_types

struct HasEmptyGetLogEntryTypesData
{
    Fixture::GetLogEntryTypesData get_log_entry_types_data;

    HasEmptyGetLogEntryTypesData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasGetLogEntryTypesDataEmptyDb
{
    Fixture::GetLogEntryTypesData get_log_entry_types_data;
    Fixture::GetLogEntryTypesResult get_log_entry_types_result;

    HasGetLogEntryTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasGetLogEntryTypesDataFullDb
{
    Fixture::GetLogEntryTypesData get_log_entry_types_data;
    Fixture::GetLogEntryTypesResult get_log_entry_types_result;

    HasGetLogEntryTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// get_object_reference_types

struct HasGetObjectReferenceTypesDataEmptyDb
{
    HasGetObjectReferenceTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasGetObjectReferenceTypesDataFullDb
{
    Fixture::GetObjectReferenceTypesResult get_object_reference_types_result;

    HasGetObjectReferenceTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// register_service

struct HasEmptyRegisterServiceData
{
    Fixture::RegisterServiceData register_service_data;

    HasEmptyRegisterServiceData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRegisterServiceDataEmptyDb
{
    Fixture::RegisterServiceData register_service_data;

    HasRegisterServiceDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// register_results

struct HasEmptyRegisterResultsData
{
    Fixture::RegisterResultsData register_results_data;

    HasEmptyRegisterResultsData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRegisterResultsDataEmptyDb
{
    Fixture::RegisterResultsData register_results_data;

    HasRegisterResultsDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRegisterResultsDataFullDb
{
    Fixture::RegisterResultsData register_results_data;

    HasRegisterResultsDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// register_log_entry_types

struct HasEmptyRegisterLogEntryTypesData
{
    Fixture::RegisterLogEntryTypesData register_log_entry_types_data;

    HasEmptyRegisterLogEntryTypesData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRegisterLogEntryTypesDataEmptyDb
{
    Fixture::RegisterLogEntryTypesData register_log_entry_types_data;

    HasRegisterLogEntryTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRegisterLogEntryTypesDataFullDb
{
    Fixture::RegisterLogEntryTypesData register_log_entry_types_data;

    HasRegisterLogEntryTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// register_object_reference_types

struct HasEmptyRegisterObjectReferenceTypesDataEmptyDb
{
    Fixture::RegisterObjectReferenceTypesData register_object_reference_types_data;

    HasEmptyRegisterObjectReferenceTypesDataEmptyDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRegisterObjectReferenceTypesDataFullDb
{
    Fixture::RegisterObjectReferenceTypesData register_object_reference_types_data;

    HasRegisterObjectReferenceTypesDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

// create_log_entry

struct HasEmptyCreateLogEntryData
{
    Fixture::CreateLogEntryData create_log_entry_data;

    HasEmptyCreateLogEntryData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasAllCreateLogEntryData
{
    Fixture::CreateLogEntryData create_log_entry_data;

    HasAllCreateLogEntryData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRequiredCreateLogEntryData
{
    Fixture::CreateLogEntryData create_log_entry_data;

    HasRequiredCreateLogEntryData(const LibPg::PgRwTransaction& _rw_transaction);
};

// close_log_entry

struct HasCloseLogEntryDataOfNonexistentLogEntry
{
    Fixture::CloseLogEntryData close_log_entry_data;

    HasCloseLogEntryDataOfNonexistentLogEntry(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasAllCloseLogEntryData
{
    Fixture::CloseLogEntryData close_log_entry_data;

    HasAllCloseLogEntryData(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasRequiredCloseLogEntryData
{
    Fixture::CloseLogEntryData close_log_entry_data;

    HasRequiredCloseLogEntryData(const LibPg::PgRwTransaction& _rw_transaction);
};

//

struct HasNoLogEntries
{
    static constexpr unsigned int count_limit = 5;
    const ::Fred::LibLogger::LogEntry::TimeEnd time_end;
    const ::Fred::LibLogger::LogEntry::TimeBegin time_begin;
    const ::Fred::LibLogger::LogEntry::ServiceName service;
    const std::vector<::Fred::LibLogger::LogEntry::LogEntryTypeName> log_entry_types;

    HasNoLogEntries(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasSeveralLogEntries
{
    const static constexpr unsigned int count_limit = 5;
    const ::Fred::LibLogger::LogEntry::TimeEnd time_end;
    const ::Fred::LibLogger::LogEntry::TimeBegin time_begin;
    const ::Fred::LibLogger::LogEntry::ServiceName service;
    const std::vector<::Fred::LibLogger::LogEntry::LogEntryTypeName> log_entry_types;

    HasSeveralLogEntries(const LibPg::PgRwTransaction& _rw_transaction);
};

struct CleanDb
{
    CleanDb(const LibPg::PgRwTransaction& _rw_transaction);
};

struct HasGetLogEntryInfoDataFullDb
{
    CleanDb clean_db;
    std::tuple<::Fred::LibLogger::LogEntryIdent, ::Fred::LibLogger::LogEntryInfo> minimal_log_entry_data;
    std::tuple<::Fred::LibLogger::LogEntryIdent, ::Fred::LibLogger::LogEntryInfo> full_log_entry_data;
    std::tuple<::Fred::LibLogger::LogEntryIdent, ::Fred::LibLogger::LogEntryInfo> nonexistent_log_entry_data;

    HasGetLogEntryInfoDataFullDb(const LibPg::PgRwTransaction& _rw_transaction);
};

} // namespace Test::Fixture
} // namespace Test

#endif

/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TestBackendLogger

#include "setup/fixture/cfg.hh"
#include "util.hh"

#include "libpg/detail/libpq_layer_impl.hh"
#include "libpg/fixed_size_query_arguments.hh"
#include "libpg/libpq_layer.hh"
#include "libpg/pg_connection.hh"
#include "libpg/pg_result.hh"
#include "libpg/util/strong_type_operators.hh"

#include "liblog/liblog.hh"
#include "liblog/log.hh"
#include "liblog/log_config.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/variant/static_visitor.hpp>

#include <cstdlib>
#include <iostream>

namespace {

enum class LogHandler
{
    is_set,
    is_not_set
};

class SetLogHandler : public boost::static_visitor<LogHandler>
{
public:
    explicit SetLogHandler(LibLog::LogConfig& liblog_log_config)
        : liblog_log_config_{liblog_log_config}
    {
    }
    LogHandler operator()(const boost::blank&)const
    {
        return LogHandler::is_not_set;
    }
    LogHandler operator()(const Test::Setup::Fixture::Cfg::Options::Log::Console& option) const
    {
        auto console_sink_config =
                LibLog::Sink::ConsoleSinkConfig()
                        .set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr);
        if (option.min_severity != boost::none)
        {
            console_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(console_sink_config);
        return LogHandler::is_set;
    }
    LogHandler operator()(const Test::Setup::Fixture::Cfg::Options::Log::Logfile& option) const
    {
        auto file_sink_config =
                LibLog::Sink::FileSinkConfig(option.file_name);
        if (option.min_severity != boost::none)
        {
            file_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(file_sink_config);
        return LogHandler::is_set;
    }
    LogHandler operator()(const Test::Setup::Fixture::Cfg::Options::Log::Syslog& option) const
    {
        auto syslog_sink_config =
                LibLog::Sink::SyslogSinkConfig()
                        .set_syslog_facility(option.facility);
        if (option.min_severity != boost::none)
        {
            syslog_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(syslog_sink_config);
        return LogHandler::is_set;
    }
private:
    LibLog::LogConfig& liblog_log_config_;
};

void show_query_argument_value(std::ostream& out, int idx, const char* value)
{
    if (idx != 0)
    {
        out << ", ";
    }
    out << "$" << (idx + 1) << ":";
    if (value == nullptr)
    {
        out << "NULL";
    }
    else
    {
        out << "\"" << value << "\"";
    }
}

auto values_to_string(const char* const* values, int number_of_values)
{
    std::ostringstream out;
    out << "[";
    for (int idx = 0; idx < number_of_values; ++idx)
    {
        show_query_argument_value(out, idx, values[idx]);
    }
    out << "]";
    return std::move(out).str();
}

class LibPgLogging
{
public:
    LibPgLogging()
    {
        LibPg::set_libpq_layer(static_cast<LibPg::LibPqLayer*>(&my_libpq_layer_impl_));
    }
    ~LibPgLogging()
    {
        LibPg::set_libpq_layer(nullptr);
    }
private:
    class LibPq : public LibPg::Detail::LibPqLayerSimpleImplementation
    {
    public:
        LibPq()
        {
        }
        ~LibPq() override
        {
        }
    private:
        using Parent = LibPg::Detail::LibPqLayerSimpleImplementation;
        ::PGconn* PQconnectdbParams(
                const char* const* keywords,
                const char* const* values,
                int expand_dbname) const override
        {
            auto* const conn = this->Parent::PQconnectdbParams(keywords, values, expand_dbname);
            this->PQsetNoticeProcessor(
                    conn,
                    [](void*, const char* message) {
                        // notice|warning messages from PostgreSQL
                        LIBLOG_WARNING((std::string{message, std::strlen(message) - 1}));
                    },
                    nullptr);
            return conn;
        }
        ::PGresult* PQexec(::PGconn* conn, const char* query) const override
        {
            LIBLOG_DEBUG("query: {}", query);
            return this->Parent::PQexec(conn, query);
        }
        ::PGresult* PQexecParams(
                ::PGconn* conn,
                const char* command,
                int nParams,
                const ::Oid* paramTypes,
                const char* const* paramValues,
                const int* paramLengths,
                const int* paramFormats,
                int resultFormat) const override
        {
            LIBLOG_DEBUG("query: {} {}", command, values_to_string(paramValues, nParams));
            return this->Parent::PQexecParams(conn, command, nParams, paramTypes, paramValues, paramLengths, paramFormats, resultFormat);
        }
    };
    LibPq my_libpq_layer_impl_;
};

std::vector<boost::asio::ip::address> get_monitoring_ips_from_file(const std::string& _filename)
{
    LIBLOG_DEBUG("get_monitoring_ips_from_file: {}", _filename);
    std::vector<boost::asio::ip::address> monitoring_ips;

    std::ifstream infile{_filename};

    std::string ipstr;
    while (infile >> ipstr)
    {

        boost::system::error_code boost_error_code;
        const auto ip = boost::asio::ip::address::from_string(ipstr, boost_error_code);
        if (boost_error_code)
        {
            LIBLOG_WARNING("INVALID monitoring ip: {}", ipstr);
        }

        monitoring_ips.emplace_back(ip);
        LIBLOG_DEBUG("monitoring ip: {} (imported as {})", ipstr, ip.to_string());
    }
    return monitoring_ips;
}

struct GlobalConfigFixture
{
    GlobalConfigFixture()
    {
        try
        {
            Test::Setup::Fixture::Cfg::Options::init(
                    boost::unit_test::framework::master_test_suite().argc,
                    boost::unit_test::framework::master_test_suite().argv);
        }
        catch (const Test::Setup::Fixture::Cfg::HelpRequested& help)
        {
            std::cout << help.what() << std::endl;
            std::exit(EXIT_SUCCESS);
        }
        catch (const Test::Setup::Fixture::Cfg::UnknownOption& unknown_option)
        {
            std::cerr << unknown_option.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (const Test::Setup::Fixture::Cfg::MissingOption& missing_option)
        {
            std::cerr << missing_option.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (const Test::Setup::Fixture::Cfg::Exception& e)
        {
            std::cerr << "Configuration problem: " << e.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Exception caught: " << e.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "Unexpected exception caught" << std::endl;
            std::exit(EXIT_FAILURE);
        }

        constexpr LibLog::ThreadMode threading = LibLog::ThreadMode::multi_threaded; // global, compile-time parameter
        std::unique_ptr<LibPgLogging> libpq_log_layer;
        auto liblog_log_config = LibLog::LogConfig();
        switch (boost::apply_visitor(SetLogHandler(liblog_log_config), Test::Setup::Fixture::Cfg::Options::get().log.device))
        {
            case LogHandler::is_set:
                LibLog::Log::start<threading>(liblog_log_config);
                libpq_log_layer = std::make_unique<LibPgLogging>();
                LIBLOG_DEBUG("test-liblogger configuration successfully loaded");
                break;
            case LogHandler::is_not_set:
                std::cout << "test-liblogger configuration successfully loaded" << std::endl;
                break;
        }

        get_monitoring_ips_from_file(Test::Setup::Fixture::Cfg::Options::get().logger.monitoring_hosts_file);
    }
};

class GlobalAdminDatabaseFixture : public GlobalConfigFixture
{
public:
    GlobalAdminDatabaseFixture()
        : original_db_name_(check_dbname(*Test::Setup::Fixture::Cfg::Options::get().database.dbname)),
          temporary_copy_db_name_(check_dbname(original_db_name_ + "_tmp_copy")),
          result_db_name_(check_dbname(original_db_name_ + "_test_result")),
          db_user_(*Test::Setup::Fixture::Cfg::Options::get().database.user)
    {
        //LOGGER.debug("GlobalAdminDatabaseFixture()"
        //        ":original_db_name_(" + original_db_name_ + "),"
        //         "temporary_copy_db_name_(" + temporary_copy_db_name_ + "),"
        //         "result_db_name_(" + result_db_name_ + "),"
        //         "db_user_(" + db_user_ + ")");
        const auto connection = LibPg::PgConnection{Test::Util::get_dsn(Test::Setup::Fixture::Cfg::Options::get().admin_database)};
        force_copy_db(connection, original_db_name_, temporary_copy_db_name_, db_user_);
    }
    ~GlobalAdminDatabaseFixture()
    {
        try
        {
            const auto connection = LibPg::PgConnection{Test::Util::get_dsn(Test::Setup::Fixture::Cfg::Options::get().admin_database)};
            noexcept_force_copy_db(connection, original_db_name_, result_db_name_, db_user_);
            noexcept_force_copy_db(connection, temporary_copy_db_name_, original_db_name_, db_user_);
            noexcept_force_drop_db(connection, temporary_copy_db_name_, db_user_);
            noexcept_enable_connections(connection, result_db_name_);
            noexcept_enable_connections(connection, original_db_name_);
            noexcept_enable_connections(connection, temporary_copy_db_name_);
        }
        catch (...)
        {
            try
            {
                LIBLOG_ERROR("get_admin_connection() failed");
            }
            catch (...)
            {
            }
        }
    }

private:
    static void disable_connections_add_terminate_persistent_connections(
            const LibPg::PgConnection& _connection,
            const std::string& db_name,
            const std::string& db_user)
    {
        using QueryArguments = LibPg::FixedSizeQueryArguments<1>;
        exec(_connection,
             "UPDATE pg_database SET datallowconn=false WHERE datname = " + QueryArguments::parameter<0>().as_text(),
             LibPg::make_fixed_size_query_arguments(db_name));

        const LibPg::PgResultTuples db_result =
                exec(_connection,
                        // clang-format off
                        "SELECT column_name "
                         "FROM information_schema.columns "
                        "WHERE table_name = 'pg_stat_activity' "
                          "AND column_name IN ('procpid', 'pid')");
                        // clang-format on
        if (db_result.size() <= LibPg::RowIndex{0})
        {
            std::runtime_error("table 'pg_stat_activity' contains neither procpid nor pid column");
        }
        const auto pid_column_name = db_result[LibPg::RowIndex{0}][LibPg::ColumnIndex{0}].as<std::string>();

        {
            using QueryArguments = LibPg::FixedSizeQueryArguments<2>;
            const auto query =
                    // clang-format off
                    "SELECT pg_terminate_backend(" + pid_column_name + ") "
                      "FROM pg_stat_activity "
                     "WHERE (datname = " + QueryArguments::parameter<0>().as_text() + " OR usename = " + QueryArguments::parameter<1>().as_text() + ") "
                       "AND " + pid_column_name + " != pg_backend_pid()";
                    // clang-format on
            const auto args = LibPg::make_fixed_size_query_arguments(db_name, db_user);
            while (true)
            {
                const LibPg::PgResultTuples db_result = exec(_connection, query, args);
                if (db_result.size() == LibPg::RowIndex{0})
                {
                    break;
                }
            }
        }
    }
    static void noexcept_enable_connections(
            const LibPg::PgConnection& _connection,
            const std::string& db_name)noexcept
    {
        try
        {
            using QueryArguments = LibPg::FixedSizeQueryArguments<1>;
            exec(
                    _connection,
                    "UPDATE pg_database SET datallowconn=true WHERE datname = " + QueryArguments::parameter<0>().as_text(),
                    LibPg::make_fixed_size_query_arguments(db_name));
        }
        catch (...)
        {
            try
            {
                LIBLOG_ERROR("UPDATE pg_database failed");
            }
            catch (...)
            {
            }
        }
    }
    static const std::string& check_dbname(const std::string& db_name)
    {
        static constexpr unsigned max_postgresql_database_name_length = 63;
        if (max_postgresql_database_name_length < db_name.length())
        {
            throw std::runtime_error(
                    "db_name.length(): " + std::to_string(db_name.length()) + " > "
                    "max_postgresql_database_name_length: " + std::to_string(max_postgresql_database_name_length) + " "
                    "db_name: " + db_name);
        }
        return db_name;
    }
    static void force_drop_db(
            const LibPg::PgConnection& _connection,
            const std::string& db_name,
            const std::string& db_user)
    {
        disable_connections_add_terminate_persistent_connections(_connection, db_name, db_user);
        exec(_connection, "DROP DATABASE IF EXISTS \"" + db_name + "\"");
    }
    static void force_copy_db(
            const LibPg::PgConnection& _connection,
            const std::string& src_name,
            const std::string& dst_name,
            const std::string& db_user)
    {
        force_drop_db(_connection, dst_name, db_user);
        exec(_connection, "CREATE DATABASE \"" + dst_name + "\" TEMPLATE \"" + src_name + "\"");
    }
    static void noexcept_force_drop_db(
            const LibPg::PgConnection& _connection,
            const std::string& db_name,
            const std::string& db_user)noexcept
    {
        try
        {
            disable_connections_add_terminate_persistent_connections(_connection, db_name, db_user);
        }
        catch (...)
        {
            try
            {
                LIBLOG_ERROR("disable_connections_add_terminate_persistent_connections failed");
            }
            catch (...)
            {
            }
        }
        try
        {
            exec(_connection, "DROP DATABASE IF EXISTS \"" + db_name + "\"");
        }
        catch (...)
        {
            try
            {
                LIBLOG_ERROR("DROP DATABASE failed");
            }
            catch (...)
            {
            }
        }
    }
    static void noexcept_force_copy_db(
            const LibPg::PgConnection& _connection,
            const std::string& src_name,
            const std::string& dst_name,
            const std::string& db_user)
    {
        noexcept_force_drop_db(_connection, dst_name, db_user);
        try
        {
            exec(_connection, "CREATE DATABASE \"" + dst_name + "\" TEMPLATE \"" + src_name + "\"");
        }
        catch (...)
        {
            try
            {
                LIBLOG_ERROR("CREATE DATABASE failed");
            }
            catch (...)
            {
            }
        }
    }
    const std::string original_db_name_;
    const std::string temporary_copy_db_name_;
    const std::string result_db_name_;
    const std::string db_user_;
};

struct GlobalDatabaseFixture : GlobalAdminDatabaseFixture
{
    GlobalDatabaseFixture()
    {
    }
};

struct GlobalFixture : GlobalDatabaseFixture
{
    GlobalFixture()
    {
    }
};

} // namespace {anonymous}

BOOST_GLOBAL_FIXTURE(GlobalFixture);

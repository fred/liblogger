/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_LOG_ENTRIES_HH_61950AA8ECB2457E951DF1192E3AC9DE
#define LIST_LOG_ENTRIES_HH_61950AA8ECB2457E951DF1192E3AC9DE

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"

#include "liblogger/list_log_entries.hh"

#include <boost/optional.hpp>

#include <vector>

namespace Test {
namespace Fixture {

struct ListLogEntriesData
{
    ::Fred::LibLogger::LogEntry::ServiceName service;
    ::Fred::LibLogger::LogEntry::TimeBegin from;
    ::Fred::LibLogger::LogEntry::TimeEnd to;
    unsigned int count_limit;
    boost::optional<::Fred::LibLogger::LogEntry::UserName> username;
    boost::optional<::Fred::LibLogger::LogEntry::UserId> user_id;
    boost::optional<::Fred::LibLogger::ObjectReference> object_reference;
    std::vector<::Fred::LibLogger::LogEntry::LogEntryTypeName> log_entry_types;
};

} // namespace Test::Fixture
} // namespace Test

#endif

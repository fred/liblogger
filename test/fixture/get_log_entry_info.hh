/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_LOG_ENTRY_INFO_HH_B207693371A94A24B0753327B70F5D50
#define GET_LOG_ENTRY_INFO_HH_B207693371A94A24B0753327B70F5D50

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional/optional.hpp>

namespace Test {
namespace Fixture {

struct GetLogEntryInfoData
{
    ::Fred::LibLogger::LogEntryIdent log_entry_ident;
};

} // namespace Test::Fixture
} // namespace Test

#endif

/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_SERVICES_HH_21299102A8674E679ECE33CBB8EE6F61
#define GET_SERVICES_HH_21299102A8674E679ECE33CBB8EE6F61

#include "liblogger/log_entry.hh"

#include <vector>

namespace Test {
namespace Fixture {

struct GetServicesData
{
};

} // namespace Test::Fixture
} // namespace Test

#endif

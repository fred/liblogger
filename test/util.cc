/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "util.hh"

#include "libpg/dsn.hh"

namespace Test {
namespace Util {

LibPg::Dsn get_dsn(const Test::Setup::Fixture::Cfg::Options::Database& _option)
{
    const LibPg::Dsn dsn = [_option]() {
        LibPg::Dsn dsn;
        if (_option.host != boost::none)
        {
            dsn.host = LibPg::Dsn::Host{*_option.host};
        }
        if (_option.host_addr != boost::none)
        {
            dsn.host_addr = LibPg::Dsn::HostAddr{boost::asio::ip::address::from_string(*_option.host_addr)};
        }
        if (_option.port != boost::none)
        {
            dsn.port = LibPg::Dsn::Port{static_cast<unsigned short>(*_option.port)};
        }
        if (_option.dbname != boost::none)
        {
            dsn.db_name = LibPg::Dsn::DbName{*_option.dbname};
        }
        if (_option.user != boost::none)
        {
            dsn.user = LibPg::Dsn::User{*_option.user};
        }
        if (_option.password != boost::none)
        {
            dsn.password = LibPg::Dsn::Password{*_option.password};
        }
        if (_option.password != boost::none)
        {
            dsn.connect_timeout = LibPg::Dsn::ConnectTimeout{std::chrono::seconds{*_option.timeout}};
        }
        return dsn;
    }();
    return dsn;
}

} // namespace Test::Util
} // namespace Test

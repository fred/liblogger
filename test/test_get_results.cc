/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/get_results.hh"

#include "libpg/util/strong_type_comparison_operators.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestGetResults)

BOOST_FIXTURE_TEST_CASE(empty_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyGetResultsData>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_results(
                    fixture.transaction,
                    fixture.get_results_data.log_entry_service),
            ::Fred::LibLogger::InvalidService);
}

BOOST_FIXTURE_TEST_CASE(empty_db_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetResultsDataEmptyDb>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_results(
                    fixture.transaction,
                    fixture.get_results_data.log_entry_service),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(full_db_nonexistent_service_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetResultsDataFullDb>>)
{
    fixture.get_results_data.log_entry_service = ::Fred::LibLogger::LogEntry::ServiceName{"NONEXISTENT"};
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_results(
                    fixture.transaction,
                    fixture.get_results_data.log_entry_service),
            ::Fred::LibLogger::ServiceNotFound);
}

namespace {

template <typename Map, class = typename std::enable_if<std::is_same<Map, std::map<::Fred::LibLogger::LogEntry::ResultName, ::Fred::LibLogger::LogEntry::ResultCode>>::value>::type>
bool compare_map(Map const& lhs, Map const& rhs)
{
    return lhs.size() == rhs.size()
        && std::equal(lhs.begin(), lhs.end(),
                      rhs.begin());
}

} // namespace {anonymous}

BOOST_FIXTURE_TEST_CASE(full_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetResultsDataFullDb>>)
{
    const auto result =
            ::Fred::LibLogger::get_results(
                    fixture.transaction,
                    fixture.get_results_data.log_entry_service);

    BOOST_CHECK(compare_map(result.result_code_map, fixture.get_results_result.result_code_map));
}

BOOST_AUTO_TEST_SUITE_END() // TestGetResults

} // namespace Test

/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/register_service.hh"

#include "libpg/util/strong_type_comparison_operators.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestRegisterService)

BOOST_FIXTURE_TEST_CASE(empty_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyRegisterServiceData>>)
{
    BOOST_CHECK_THROW(
        ::Fred::LibLogger::register_service(
                fixture.transaction,
                fixture.register_service_data.service_name,
                fixture.register_service_data.service_handle),
        ::Fred::LibLogger::InvalidService);
};

BOOST_FIXTURE_TEST_CASE(empty_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterServiceDataEmptyDb>>)
{
    for (auto iteration = 0; iteration < 2; ++iteration)
    {
        BOOST_CHECK_NO_THROW(
            ::Fred::LibLogger::register_service(
                    fixture.transaction,
                    fixture.register_service_data.service_name,
                    fixture.register_service_data.service_handle));

        const auto get_services_result =
                ::Fred::LibLogger::get_services(
                        fixture.transaction);

        BOOST_CHECK_EQUAL(get_services_result.log_entry_services.size(), 1);
        BOOST_CHECK(std::find(get_services_result.log_entry_services.cbegin(), get_services_result.log_entry_services.cend(), fixture.register_service_data.service_name) != get_services_result.log_entry_services.cend());
    }
};

BOOST_AUTO_TEST_SUITE_END() // TestRegisterService

} // namespace Test

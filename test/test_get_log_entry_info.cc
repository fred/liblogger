/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "fixture/get_log_entry_info.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/get_log_entry_info.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/optional/optional_io.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/test/unit_test.hpp>

#include <string>
#include <map>
#include <stdexcept>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestGetLogEntryInfo)

BOOST_FIXTURE_TEST_CASE(service_not_found, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetLogEntryInfoDataFullDb>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_log_entry_info(
                    fixture.transaction,
                    ::Fred::LibLogger::LogEntryIdent{
                            std::get<::Fred::LibLogger::LogEntryIdent>(fixture.minimal_log_entry_data).id,
                            std::get<::Fred::LibLogger::LogEntryIdent>(fixture.minimal_log_entry_data).time_begin,
                            ::Fred::LibLogger::LogEntry::ServiceName{"NonexistentService"},
                            std::get<::Fred::LibLogger::LogEntryIdent>(fixture.minimal_log_entry_data).monitoring_status}),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(log_entry_does_not_exist, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetLogEntryInfoDataFullDb>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_log_entry_info(
                    fixture.transaction,
                    std::get<::Fred::LibLogger::LogEntryIdent>(fixture.nonexistent_log_entry_data)),
            ::Fred::LibLogger::LogEntryDoesNotExist);
}

namespace {

template <typename Map>
void check_equal_property_children(const Map& _lhs, const Map& _rhs) {
    BOOST_CHECK_EQUAL(_lhs.size(), _rhs.size());
    if (_lhs.size() == _rhs.size())
    {
        auto rhs = _rhs.cbegin();
        for (auto lhs = _lhs.cbegin(); lhs != _lhs.cend(); ++lhs)
        {
            BOOST_CHECK_EQUAL(lhs->first, rhs->first);
            BOOST_CHECK_EQUAL_COLLECTIONS(lhs->second.cbegin(), lhs->second.cend(), rhs->second.cbegin(), rhs->second.cend());
            ++rhs;
        }
    }
}

void compare_properties(const std::vector<::Fred::LibLogger::LogEntryProperty>& _lhs, const std::vector<::Fred::LibLogger::LogEntryProperty>& _rhs)
{
    BOOST_CHECK_EQUAL(_lhs.size(), _rhs.size());
    for (size_t object_property_idx = 0; object_property_idx < _lhs.size(); ++object_property_idx)
    {
        BOOST_CHECK_EQUAL(_lhs.at(object_property_idx).property_type, _rhs.at(object_property_idx).property_type);
        BOOST_CHECK_EQUAL(_lhs.at(object_property_idx).property_values.size(), _rhs.at(object_property_idx).property_values.size());
        for (size_t object_property_value_idx = 0; object_property_value_idx < _lhs.at(object_property_idx).property_values.size(); ++object_property_value_idx)
        {
            BOOST_CHECK_EQUAL(_lhs.at(object_property_idx).property_values.at(object_property_value_idx).property_value, _rhs.at(object_property_idx).property_values.at(object_property_value_idx).property_value);
            check_equal_property_children(_lhs.at(object_property_idx).property_values.at(object_property_value_idx).children, _rhs.at(object_property_idx).property_values.at(object_property_value_idx).children);
        }
    }
}

void compare_object_references(const std::vector<::Fred::LibLogger::ObjectReferences>& _lhs, const std::vector<::Fred::LibLogger::ObjectReferences>& _rhs)
{
    BOOST_CHECK_EQUAL(_lhs.size(), _rhs.size());
    for (size_t object_reference_idx = 0; object_reference_idx < _lhs.size(); ++object_reference_idx)
    {
        BOOST_CHECK_EQUAL_COLLECTIONS(_lhs.at(object_reference_idx).object_reference_ids.cbegin(), _lhs.at(object_reference_idx).object_reference_ids.cend(), _rhs.at(object_reference_idx).object_reference_ids.cbegin(), _rhs.at(object_reference_idx).object_reference_ids.cend());
    }
}

void compare_log_entry_info(const ::Fred::LibLogger::LogEntryInfo& _lhs, const ::Fred::LibLogger::LogEntryInfo& _rhs)
{
    BOOST_CHECK_EQUAL(_lhs.time_begin, _rhs.time_begin);
    BOOST_CHECK_EQUAL(_lhs.service_name, _rhs.service_name);
    BOOST_CHECK_EQUAL(_lhs.log_entry_type, _rhs.log_entry_type);
    BOOST_CHECK_EQUAL(_lhs.username, _rhs.username);
    BOOST_CHECK_EQUAL(_lhs.user_id, _rhs.user_id);
    BOOST_CHECK_EQUAL(_lhs.result_code, _rhs.result_code);
    BOOST_CHECK_EQUAL(_lhs.result_name, _rhs.result_name);
    compare_properties(_lhs.input_properties, _rhs.input_properties);
    compare_properties(_lhs.output_properties, _rhs.output_properties);
    compare_object_references(_lhs.object_references, _rhs.object_references);
}

} // namespace Test::{TestGetLogEntryInfo}::{anonymous}

BOOST_FIXTURE_TEST_CASE(full_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetLogEntryInfoDataFullDb>>)
{
    ::Fred::LibLogger::GetLogEntryInfoReply result;
    BOOST_CHECK_NO_THROW(
            result = ::Fred::LibLogger::get_log_entry_info(
                    fixture.transaction,
                    std::get<::Fred::LibLogger::LogEntryIdent>(fixture.minimal_log_entry_data)));
    compare_log_entry_info(result.log_entry_info, std::get<::Fred::LibLogger::LogEntryInfo>(fixture.minimal_log_entry_data));

    BOOST_CHECK_NO_THROW(
            result = ::Fred::LibLogger::get_log_entry_info(
                    fixture.transaction,
                    std::get<::Fred::LibLogger::LogEntryIdent>(fixture.full_log_entry_data)));
    compare_log_entry_info(result.log_entry_info, std::get<::Fred::LibLogger::LogEntryInfo>(fixture.full_log_entry_data));
}

BOOST_AUTO_TEST_SUITE_END()

} // namespace Test

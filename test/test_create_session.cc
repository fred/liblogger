/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/create_session.hh"
#include "liblogger/exceptions.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestCreateSession)

BOOST_FIXTURE_TEST_CASE(empty_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyCreateSessionData>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::create_session(
                    fixture.transaction,
                    fixture.create_session_data.username,
                    fixture.create_session_data.user_id),
            ::Fred::LibLogger::InvalidUserName);
}

BOOST_FIXTURE_TEST_CASE(all_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasAllCreateSessionData>>)
{
    //BOOST_CHECK_NO_THROW(
    const auto result =
            ::Fred::LibLogger::create_session(
                    fixture.transaction,
                    fixture.create_session_data.username,
                    fixture.create_session_data.user_id);//)
    BOOST_CHECK_GT(result.session_ident.id.get(), 0);
    BOOST_CHECK(!result.session_ident.login_date.get().is_special());
}

BOOST_FIXTURE_TEST_CASE(required_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasRequiredCreateSessionData>>)
{
    //BOOST_CHECK_NO_THROW(
    const auto result =
            ::Fred::LibLogger::create_session(
                    fixture.transaction,
                    fixture.create_session_data.username,
                    fixture.create_session_data.user_id);//)
    BOOST_CHECK_GT(result.session_ident.id.get(), 0);
    BOOST_CHECK(!result.session_ident.login_date.get().is_special());
}

BOOST_AUTO_TEST_SUITE_END() // TestCreateSession

} // namespace Test

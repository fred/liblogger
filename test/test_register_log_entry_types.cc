/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/register_log_entry_types.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestRegisterLogEntryTypes)

BOOST_FIXTURE_TEST_CASE(empty_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyRegisterLogEntryTypesData>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::register_log_entry_types(
                    fixture.transaction,
                    fixture.register_log_entry_types_data.service,
                    fixture.register_log_entry_types_data.log_entry_types),
            ::Fred::LibLogger::InvalidService);
}

BOOST_FIXTURE_TEST_CASE(empty_db_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterLogEntryTypesDataEmptyDb>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::register_log_entry_types(
                    fixture.transaction,
                    fixture.register_log_entry_types_data.service,
                    fixture.register_log_entry_types_data.log_entry_types),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(full_db_nonexistent_service_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterLogEntryTypesDataFullDb>>)
{
    fixture.register_log_entry_types_data.service = ::Fred::LibLogger::LogEntry::ServiceName{"NONEXISTENT"};
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::register_log_entry_types(
                    fixture.transaction,
                    fixture.register_log_entry_types_data.service,
                    fixture.register_log_entry_types_data.log_entry_types),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(full_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterLogEntryTypesDataFullDb>>)
{
    for (auto iteration = 0; iteration < 2; ++iteration)
    {
        BOOST_CHECK_NO_THROW(
                ::Fred::LibLogger::register_log_entry_types(
                        fixture.transaction,
                        fixture.register_log_entry_types_data.service,
                        fixture.register_log_entry_types_data.log_entry_types));

        const auto get_log_entry_types_result =
                ::Fred::LibLogger::get_log_entry_types(
                        fixture.transaction,
                        fixture.register_log_entry_types_data.service);

        BOOST_CHECK(!get_log_entry_types_result.log_entry_types.empty());
        BOOST_CHECK_EQUAL(get_log_entry_types_result.log_entry_types.size(), fixture.register_log_entry_types_data.log_entry_types.size());
        for (auto& log_entry_type : fixture.register_log_entry_types_data.log_entry_types)
        {
            BOOST_CHECK(std::find(get_log_entry_types_result.log_entry_types.cbegin(), get_log_entry_types_result.log_entry_types.cend(), log_entry_type) != get_log_entry_types_result.log_entry_types.cend());
        }
    }
}

BOOST_AUTO_TEST_SUITE_END() // TestRegisterLogEntryTypes

} // namespace Test

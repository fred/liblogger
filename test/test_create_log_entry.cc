/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/create_log_entry.hh"
#include "liblogger/exceptions.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestCreateLogEntry)

BOOST_FIXTURE_TEST_CASE(empty_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyCreateLogEntryData>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::create_log_entry(
                    fixture.transaction,
                    fixture.create_log_entry_data.ip,
                    ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                    fixture.create_log_entry_data.log_entry_service,
                    fixture.create_log_entry_data.log_entry_content,
                    fixture.create_log_entry_data.log_entry_properties,
                    fixture.create_log_entry_data.object_references,
                    fixture.create_log_entry_data.log_entry_type,
                    fixture.create_log_entry_data.session_ident),
            ::Fred::LibLogger::InvalidService);
}

BOOST_FIXTURE_TEST_CASE(required_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasRequiredCreateLogEntryData>>)
{
    BOOST_CHECK_NO_THROW(
            const auto create_log_entry_result =
                    ::Fred::LibLogger::create_log_entry(
                            fixture.transaction,
                            fixture.create_log_entry_data.ip,
                            ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                            fixture.create_log_entry_data.log_entry_service,
                            fixture.create_log_entry_data.log_entry_content,
                            fixture.create_log_entry_data.log_entry_properties,
                            fixture.create_log_entry_data.object_references,
                            fixture.create_log_entry_data.log_entry_type,
                            fixture.create_log_entry_data.session_ident);

            // const auto get_log_entry_info_result =
                    ::Fred::LibLogger::get_log_entry_info(
                            fixture.transaction,
                            create_log_entry_result.log_entry_ident));
}

BOOST_FIXTURE_TEST_CASE(all_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasAllCreateLogEntryData>>)
{
    BOOST_CHECK_NO_THROW(
            const auto create_log_entry_result =
                    ::Fred::LibLogger::create_log_entry(
                            fixture.transaction,
                            fixture.create_log_entry_data.ip,
                            ::Fred::LibLogger::LogEntry::IsMonitoring{false},
                            fixture.create_log_entry_data.log_entry_service,
                            fixture.create_log_entry_data.log_entry_content,
                            fixture.create_log_entry_data.log_entry_properties,
                            fixture.create_log_entry_data.object_references,
                            fixture.create_log_entry_data.log_entry_type,
                            fixture.create_log_entry_data.session_ident);

            // const auto get_log_entry_info_result =
                    ::Fred::LibLogger::get_log_entry_info(
                            fixture.transaction,
                            create_log_entry_result.log_entry_ident));
}

BOOST_AUTO_TEST_SUITE_END() // TestCloseSession

} // namespace Test

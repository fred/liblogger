/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/get_results.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/test/unit_test.hpp>

#include <map>
#include <tuple>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestRegisterResults)

BOOST_FIXTURE_TEST_CASE(empty_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyRegisterResultsData>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::register_results(
                    fixture.transaction,
                    fixture.register_results_data.service,
                    fixture.register_results_data.results),
            ::Fred::LibLogger::InvalidService);
}

BOOST_FIXTURE_TEST_CASE(empty_db_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterResultsDataEmptyDb>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::register_results(
                    fixture.transaction,
                    fixture.register_results_data.service,
                    fixture.register_results_data.results),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(full_db_nonexistent_service_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterResultsDataFullDb>>)
{
    fixture.register_results_data.service = ::Fred::LibLogger::LogEntry::ServiceName{"NONEXISTENT"};
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::register_results(
                    fixture.transaction,
                    fixture.register_results_data.service,
                    fixture.register_results_data.results),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(full_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasRegisterResultsDataFullDb>>)
{
    for (auto iteration = 0; iteration < 2; ++iteration)
    {
        BOOST_CHECK_NO_THROW(
                ::Fred::LibLogger::register_results(
                        fixture.transaction,
                        fixture.register_results_data.service,
                        fixture.register_results_data.results));

        const auto get_results_result =
                ::Fred::LibLogger::get_results(
                        fixture.transaction,
                        fixture.register_results_data.service);

        BOOST_CHECK(!get_results_result.result_code_map.empty());
        BOOST_CHECK_EQUAL(get_results_result.result_code_map.size(), fixture.register_results_data.results.size());
        for (auto& result : fixture.register_results_data.results)
        {
            const auto found_result = get_results_result.result_code_map.find(result.name);
            BOOST_CHECK(found_result != get_results_result.result_code_map.cend());
            BOOST_CHECK_EQUAL(found_result->first, result.name);
            BOOST_CHECK_EQUAL(found_result->second, result.code);
        }
    }
}

BOOST_AUTO_TEST_SUITE_END() // TestRegisterResults

} // namespace Test

/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fixture.hh"
#include "util.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/get_log_entry_types.hh"

#include "libpg/util/strong_type_operators.hh"

#include <boost/test/unit_test.hpp>

namespace Test {

BOOST_AUTO_TEST_SUITE(TestGetLogEntryTypes)

BOOST_FIXTURE_TEST_CASE(empty_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasEmptyGetLogEntryTypesData>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_log_entry_types(
                    fixture.transaction,
                    fixture.get_log_entry_types_data.log_entry_service),
            ::Fred::LibLogger::InvalidService);
}

BOOST_FIXTURE_TEST_CASE(empty_db_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetLogEntryTypesDataEmptyDb>>)
{
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_log_entry_types(
                    fixture.transaction,
                    fixture.get_log_entry_types_data.log_entry_service),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(full_db_nonexistent_service_fail, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetLogEntryTypesDataFullDb>>)
{
    fixture.get_log_entry_types_data.log_entry_service = ::Fred::LibLogger::LogEntry::ServiceName{"NONEXISTENT"};
    BOOST_CHECK_THROW(
            ::Fred::LibLogger::get_log_entry_types(
                    fixture.transaction,
                    fixture.get_log_entry_types_data.log_entry_service),
            ::Fred::LibLogger::ServiceNotFound);
}

BOOST_FIXTURE_TEST_CASE(full_db_ok, Util::AddFixture<Util::SupplyCtx<Fixture::HasGetLogEntryTypesDataFullDb>>)
{
    const auto result =
            ::Fred::LibLogger::get_log_entry_types(
                    fixture.transaction,
                    fixture.get_log_entry_types_data.log_entry_service);

    //BOOST_CHECK_EQUAL(result.log_entry_types, fixture.get_log_entry_types_result.log_entry_types);
    BOOST_CHECK_EQUAL(result.log_entry_types.size(), fixture.get_log_entry_types_result.log_entry_types.size());
    for (std::size_t idx = 0; idx != result.log_entry_types.size(); ++idx)
    {
        BOOST_CHECK_EQUAL(result.log_entry_types.at(idx), fixture.get_log_entry_types_result.log_entry_types.at(idx));
    }
}

BOOST_AUTO_TEST_SUITE_END() // TestGetLogEntryTypes

} // namespace Test

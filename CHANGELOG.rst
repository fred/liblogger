ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

2.1.0 (2024-04-15)
------------------

* Changed implementation of register_* methods so that the database
  sequence is not always incremented if service already exists

2.0.2 (2024-03-11)
------------------

* Fixed service name in get_log_entry_info

2.0.1 (2024-02-22)
------------------

* Return service name in get_log_entry_info as is (not in lowercase)

2.0.0 (2023-10-04)
------------------

* List log entries also by properties

1.5.4 (2022-04-07)
------------------

* Normalize service name in log entry ident
* Rename service_handle to service_name
* Remove spaces from service name parameter in SQL

1.5.3 (2022-03-25)
------------------

* Fix SQL bug

1.5.2 (2022-03-24)
------------------

* Fix bug in create_log_entry
* Change SessionIdent
* Use ``LOWER(name)`` index in ``service`` table

1.5.1 (2022-03-22)
------------------
* Use LibStrong::Optional

1.5.0 (2022-03-10)
------------------

* Update CI
* Switch to ``liblog@1.4.2``
* Switch to ``libpg@2.2.0``
* Integrate ``libstrong`` library
* Simplify composed log entry id format (implementation detail)
* Modify ``LogEntryInfo`` structure:

  * Add ``log_entry_ident``
  * Add ``session_ident``


1.4.0 (2020-12-10)
------------------

* Switch to ``liblog@1.2.0``
* Rename changelog to CHANGELOG.rst to match all FRED projects


1.3.0 (2020-09-18)
------------------

* Log unexpected number of args (warning)
* Improve SQL formatting
* Improve CMake (project version)


1.2.0 (2020-06-09)
------------------

* New functions to register entities:

  * ``register_service``
  * ``register_results``
  * ``register_log_entry_types``
  * ``register_object_reference_types``


1.1.0 (2020-06-05)
------------------

* Changed ``get_log_entry_info``

  * Separate input/output log entry properties


1.0.0 (2020-05-15)
------------------

* Initial ``LibLogger`` implementation extracted from ``Logger``

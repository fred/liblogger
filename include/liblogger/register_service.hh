/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTER_SERVICE_HH_27827EDA8FF74B5197BEED44DF9A429E
#define REGISTER_SERVICE_HH_27827EDA8FF74B5197BEED44DF9A429E

#include "liblogger/log_entry.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace LibLogger {

void register_service(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::ServiceName& _service_name,
        const LogEntry::ServiceHandle& _service_handle);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

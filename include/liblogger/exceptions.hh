/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EXCEPTIONS_HH_A3F5CE6885C3476BAAB55C7EEC9F2DF9
#define EXCEPTIONS_HH_A3F5CE6885C3476BAAB55C7EEC9F2DF9

#include <exception>

namespace Fred {
namespace LibLogger {

struct SessionDoesNotExist : std::exception
{
    const char* what() const noexcept override;
};

struct SessionAlreadyClosed : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidService : std::exception
{
    const char* what() const noexcept override;
};

struct ServiceNotFound : std::exception
{
    const char* what() const noexcept override;
};

struct ServiceExists : std::exception
{
    const char* what() const noexcept override;
};

struct ServiceRegistrationFailed : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidTimestampFrom : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidTimestampTo : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidTimestampRange : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidCountLimit : std::exception
{
    const char* what() const noexcept override;
};

struct ListLogEntriesException : std::exception
{
    const char* what() const noexcept override;
};

struct LogEntryDoesNotExist : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidLogEntryIdent : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidLogEntryType : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidSessionIdent : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidUserName : std::exception
{
    const char* what() const noexcept override;
};

struct LogEntryTypeNotFound : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidPropertyType : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidObjectType : std::exception
{
    const char* what() const noexcept override;
};

struct ObjectTypeNotFound : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidResultName : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidResult : std::exception
{
    const char* what() const noexcept override;
};

struct ResultCodeNotFound : std::exception
{
    const char* what() const noexcept override;
};

} // namespace Fred::LibLogger
} // namespace Fred

#endif

/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_LOG_ENTRY_HH_46597AE5B9074072B82EF7CCAC9DCA78
#define CREATE_LOG_ENTRY_HH_46597AE5B9074072B82EF7CCAC9DCA78

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"
#include "liblogger/session_ident.hh"

#include "libpg/pg_rw_transaction.hh"

#include <boost/optional.hpp>

#include <vector>

namespace Fred {
namespace LibLogger {

struct CreateLogEntryReply
{
    LogEntryIdent log_entry_ident;
};

CreateLogEntryReply create_log_entry(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LibStrong::Optional<LogEntry::SourceIp>& _ip,
        const LogEntry::IsMonitoring& _is_monitoring,
        const LogEntry::ServiceName& _log_entry_service,
        const LibStrong::Optional<LogEntry::Content>& _log_entry_content,
        const std::vector<LogEntryProperty>& _log_entry_properties,
        const std::vector<ObjectReferences>& _object_references,
        const LogEntry::LogEntryTypeName& _log_entry_type,
        const boost::optional<SessionIdent>& _session_ident);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

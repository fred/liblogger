/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTER_OBJECT_REFERENCE_TYPES_HH_B95F4F768D3A424F88D040B8949186DE
#define REGISTER_OBJECT_REFERENCE_TYPES_HH_B95F4F768D3A424F88D040B8949186DE

#include "liblogger/log_entry.hh"

#include "libpg/pg_rw_transaction.hh"

#include <vector>

namespace Fred {
namespace LibLogger {

bool is_valid(const LogEntry::LogEntryObjectTypeName& _object_reference_type);

void register_object_reference_types(
        const LibPg::PgRwTransaction& _rw_transaction,
        const std::vector<LogEntry::LogEntryObjectTypeName>& _object_reference_types);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

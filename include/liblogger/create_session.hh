/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_SESSION_HH_A44F7A87E77E4A949285FB486F3EF7BD
#define CREATE_SESSION_HH_A44F7A87E77E4A949285FB486F3EF7BD

#include "liblogger/log_entry.hh"
#include "liblogger/session_ident.hh"

#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace LibLogger {

struct CreateSessionReply
{
    SessionIdent session_ident;
};

CreateSessionReply create_session(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::UserName& _user_name,
        const LibStrong::Optional<LogEntry::UserId>& _user_id);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

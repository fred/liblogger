/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_SERVICES_HH_BAE4C2424212423597674BED4007EE47
#define GET_SERVICES_HH_BAE4C2424212423597674BED4007EE47

#include "liblogger/log_entry.hh"

#include "libpg/pg_transaction.hh"

#include <vector>

namespace Fred {
namespace LibLogger {

struct GetServicesReply
{
    std::vector<LogEntry::ServiceName> log_entry_services;
};

GetServicesReply get_services(
        const LibPg::PgTransaction& _transaction);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

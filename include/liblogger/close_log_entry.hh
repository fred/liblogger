/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLOSE_LOG_ENTRY_HH_AC73FCB3D2304B37B8B64EDFD2F8C635
#define CLOSE_LOG_ENTRY_HH_AC73FCB3D2304B37B8B64EDFD2F8C635

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"
#include "liblogger/session_ident.hh"

#include "libpg/pg_rw_transaction.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <vector>

namespace Fred {
namespace LibLogger {

void close_log_entry(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntryIdent& _log_entry_ident,
        const LibStrong::Optional<LogEntry::Content>& _log_entry_content,
        const std::vector<LogEntryProperty>& _log_entry_properties,
        const std::vector<ObjectReferences>& _object_references,
        const boost::variant<LogEntry::ResultCode, LogEntry::ResultName>& _result_code_or_name,
        const boost::optional<SessionIdent>& _session_ident);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

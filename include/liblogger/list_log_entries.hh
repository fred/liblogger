/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_LOG_ENTRIES_HH_9DF8A1174811428CBF29B838070EE9F0
#define LIST_LOG_ENTRIES_HH_9DF8A1174811428CBF29B838070EE9F0

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"

#include "libpg/pg_transaction.hh"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional.hpp>

#include <map>
#include <vector>

namespace Fred {
namespace LibLogger {

struct ObjectReference
{
    LogEntry::LogEntryObjectTypeName object_type;
    LogEntry::ObjectIdent object_ident;
};

struct ListLogEntriesReply
{
    std::vector<LogEntryIdent> log_entry_ids;
};

ListLogEntriesReply list_log_entries(
        const LibPg::PgTransaction& _transaction,
        const LogEntry::ServiceName& _service,
        const LogEntry::TimeBegin& _from,
        const LogEntry::TimeEnd& _to,
        unsigned int _count_limit,
        const LibStrong::Optional<LogEntry::UserName>& _username,
        const LibStrong::Optional<LogEntry::UserId>& _user_id,
        const boost::optional<ObjectReference>& _object_reference,
        const std::vector<LogEntry::LogEntryTypeName>& _log_entry_types,
        const std::map<LogEntry::PropertyType, std::vector<LogEntry::PropertyValue>>& _properties);


} // namespace Fred::LibLogger
} // namespace Fred

#endif

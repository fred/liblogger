/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_RESULTS_HH_F7EA5D5CD86B468D84018D272B350E55
#define GET_RESULTS_HH_F7EA5D5CD86B468D84018D272B350E55

#include "liblogger/log_entry.hh"

#include "libpg/pg_transaction.hh"

#include <map>

namespace Fred {
namespace LibLogger {

struct GetResultsReply
{
    std::map<LogEntry::ResultName, LogEntry::ResultCode> result_code_map;
};

GetResultsReply get_results(
        const LibPg::PgTransaction& _transaction,
        const LogEntry::ServiceName& _log_entry_service);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

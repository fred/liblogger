/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_LOG_ENTRY_INFO_HH_5908539F69744E328FA8A444800172E3
#define GET_LOG_ENTRY_INFO_HH_5908539F69744E328FA8A444800172E3

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"
#include "liblogger/session_ident.hh"

#include "libpg/pg_transaction.hh"

#include "libstrong/type.hh"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional.hpp>

#include <vector>


namespace Fred {
namespace LibLogger {

struct LogEntryInfo
{
    LogEntryIdent log_entry_ident;
    LogEntry::TimeBegin time_begin;
    LibStrong::Optional<LogEntry::TimeEnd> time_end;
    LibStrong::Optional<LogEntry::SourceIp> source_ip;
    LibStrong::Optional<LogEntry::LogEntryTypeName> log_entry_type;
    LibStrong::Optional<LogEntry::ResultCode> result_code;
    LibStrong::Optional<LogEntry::ResultName> result_name;
    LibStrong::Optional<LogEntry::RawRequest> raw_request;
    LibStrong::Optional<LogEntry::RawResponse> raw_response;
    LibStrong::Optional<LogEntry::UserName> username;
    LibStrong::Optional<LogEntry::UserId> user_id;
    std::vector<LogEntryProperty> input_properties;
    std::vector<LogEntryProperty> output_properties;
    std::vector<ObjectReferences> object_references;
    boost::optional<SessionIdent> session_ident;
};

struct GetLogEntryInfoReply
{
    LogEntryInfo log_entry_info;
};

GetLogEntryInfoReply get_log_entry_info(
        const LibPg::PgTransaction& _transaction,
        const LogEntryIdent& _log_entry_ident);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

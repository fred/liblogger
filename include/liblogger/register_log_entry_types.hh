/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTER_LOG_ENTRY_TYPES_HH_173F3999255D4CF1B90A8F5D193ABC7A
#define REGISTER_LOG_ENTRY_TYPES_HH_173F3999255D4CF1B90A8F5D193ABC7A

#include "liblogger/log_entry.hh"

#include "libpg/pg_rw_transaction.hh"

#include <vector>

namespace Fred {
namespace LibLogger {

bool is_valid(const LogEntry::LogEntryTypeName& _log_entry_type);

void register_log_entry_types(
        const LibPg::PgRwTransaction& _rw_transaction,
        const LogEntry::ServiceName& _service,
        const std::vector<LogEntry::LogEntryTypeName>& _log_entry_types);

} // namespace Fred::LibLogger
} // namespace Fred

#endif

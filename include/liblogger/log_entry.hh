/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOG_ENTRY_HH_C42FFA9E17BE44D48FAB7F5FB13A9E02
#define LOG_ENTRY_HH_C42FFA9E17BE44D48FAB7F5FB13A9E02

#include "libpg/pg_rw_transaction.hh"

#include "libstrong/type.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/ptime.hpp>

#include <string>
#include <tuple>
#include <vector>

namespace Fred {
namespace LibLogger {

namespace LogEntry {

using TimeBeginDate = LibStrong::Type<boost::gregorian::date, struct TimeBeginDateTag_>;
using TimeBegin = LibStrong::Type<boost::posix_time::ptime, struct TimeBeginTag_>;
using TimeEnd = LibStrong::Type<boost::posix_time::ptime, struct TimeEndTag_>;
using SourceIp = LibStrong::Type<boost::asio::ip::address, struct SourceIpTag_>;
using ResultCode = LibStrong::TypeWithSkills<unsigned long long, struct ResultCodeTag_, LibStrong::Skill::Comparable>;
using ResultName = LibStrong::TypeWithSkills<std::string, struct ResultNameTag_, LibStrong::Skill::Comparable>;

using SessionId = LibStrong::ArithmeticSequence<unsigned long long, struct SessionIdTag_>;
using SessionLoginDate = LibStrong::Type<boost::gregorian::date, struct SessionLoginDateTag_>;
using SessionLoginTime = LibStrong::Type<boost::posix_time::ptime, struct SessionLoginTimeTag_>;
using UserName = LibStrong::TypeWithSkills<std::string, struct UserNameTag_, LibStrong::Skill::Comparable>;
using UserId = LibStrong::ArithmeticSequence<unsigned long long, struct UserIdTag_>;

using ServiceId = LibStrong::ArithmeticSequence<int, struct ServiceIdTag_>;
using ServiceName = LibStrong::TypeWithSkills<std::string, struct ServiceNameTag_, LibStrong::Skill::Comparable>;
using ServiceHandle = LibStrong::Type<std::string, struct ServiceHandleTag_>;
using IsMonitoring = LibStrong::Type<bool, struct IsMonitoringTag_>;
using LogEntryId = LibStrong::ArithmeticSequence<unsigned long long, struct LogEntryIdTag_>;
using Content = LibStrong::Type<std::string, struct ContentTag_>;
using RawRequest = LibStrong::Type<std::string, struct RawRequestTag_>;
using RawResponse = LibStrong::Type<std::string, struct RawResponseTag_>;
using IsResponse = LibStrong::Type<bool, struct IsResponseTag_>;

using PropertyId = LibStrong::ArithmeticSequence<unsigned long long, struct PropertyIdTag_>;
using PropertyNameId = LibStrong::ArithmeticSequence<int, struct PropertyNameIdTag_>;
using PropertyType = LibStrong::Type<std::string, struct PropertyTypeTag_, LibStrong::Skill::Comparable>;
using PropertyValue = LibStrong::Type<std::string, struct PropertyValueTag_>;
using IsOutput = LibStrong::Type<bool, struct IsOutputTag_, LibStrong::Skill::Comparable>;

using LogEntryObjectTypeName = LibStrong::Type<std::string, struct LogEntryObjectTypeNameTag_, LibStrong::Skill::Comparable>;
using LogEntryObjectTypeId = LibStrong::ArithmeticSequence<unsigned int, struct LogEntryObjectTypeIdTag_>;
using ObjectId = LibStrong::ArithmeticSequence<int, struct ObjectIdTag_>;
using ObjectIdent = LibStrong::Type<std::string, struct ObjectIdentTag_>;

using LogEntryTypeName = LibStrong::Type<std::string, struct LogEntryTypeNameTag_>;
using LogEntryTypeId = LibStrong::ArithmeticSequence<unsigned int, struct LogEntryTypeIdTag_>;

using CorbaImplCompatibility = LibStrong::Type<bool, struct CorbaImplCompatibilityTag_>;

} // namespace Fred::LibLogger::LogEntry

struct LogEntryPropertyValue
{
    LogEntryPropertyValue() = default;

    LogEntryPropertyValue(
            LogEntry::PropertyValue _property_value,
            std::map<LogEntry::PropertyType, std::vector<LogEntry::PropertyValue>> _children);

    LogEntry::PropertyValue property_value;
    std::map<LogEntry::PropertyType, std::vector<LogEntry::PropertyValue>> children;
};

struct LogEntryProperty
{
    LogEntryProperty() = default;

    LogEntryProperty(
            LogEntry::PropertyType _property_type,
            std::vector<LogEntryPropertyValue> _property_values);

    LogEntry::PropertyType property_type;
    std::vector<LogEntryPropertyValue> property_values;
};

struct ObjectReferences
{
    ObjectReferences() = default;

    ObjectReferences(
            LogEntry::LogEntryObjectTypeName _object_type,
            std::vector<LogEntry::ObjectIdent> _object_reference_ids);

    LogEntry::LogEntryObjectTypeName object_type;
    std::vector<LogEntry::ObjectIdent> object_reference_ids;
};

struct WrapperError : std::exception { };
struct UnwrapperError : std::exception { };

} // namespace Fred::LibLogger
} // namespace Fred

#endif

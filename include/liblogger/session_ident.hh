/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SESSION_IDENT_HH_F8BE856802B0484B8600254FC58AD1A7
#define SESSION_IDENT_HH_F8BE856802B0484B8600254FC58AD1A7

#include "liblogger/log_entry.hh"

namespace Fred {
namespace LibLogger {

struct SessionIdent
{
    SessionIdent(
            const LogEntry::SessionId& _id,
            const LogEntry::SessionLoginDate& _login_date);
    SessionIdent(
            const LogEntry::SessionId& _id,
            const LogEntry::SessionLoginTime& _login_time);

    const LogEntry::SessionId id;
    const LogEntry::SessionLoginDate login_date;
};

} // namespace Fred::LibLogger
} // namespace Fred

#endif
